﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{

    // animated sprites
    public enum AnimSpriteType
    {
        Spawn
    }

    public class Gameplay : GameScreen
    {
        //standard variables
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //fonts
        SpriteFont font1, font2, font3;

        #region Viewports
        //viewports
        List<Viewport> viewports;
        Viewport player1Viewport;
        Viewport player2Viewport;
        Viewport player3Viewport;
        Viewport player4Viewport;

        // the incoming and outgoing viewport
        Viewport masterViewport;
        #endregion
        
        //track manager
         TrackManager trackManager;

        //stats HUD (for testing purposes)
        bool showStats = false;

        //players
        int players;
        int hPlayers;
        int aiPlayers;
        List<float> ladder;

        //tracking frames per second
        float frames;
        double elapsed;
        float fps;

        #region Objects
        //objects
        Model shipModel;
        List<Ship> ships; //fill with all ships

        //ships cone model
        Model coneModel;
        List<Texture2D> coneTextures;
        Model checkeredLine;

        SoundEffect soundEngine;

        //skybox
        Texture2D background;
        Skybox skybox;

        // animated sprite manager
        AnimSpriteManager animatedSprite;

        // animated sprite texture files (matches AnimSpriteType)
        String[] animatedSpriteFiles = new String[] { "SpawnGrid_16" };
        Texture2D[] animatedSpriteTextures;

        //actived powerups
        public List<MineDrop> mines;
        public List<RocketDrop> rockets;
        #endregion

        //input states, gamepad states all held in a single array
        GamePadState[] gamePads = new GamePadState[4];

        //game state variable
        public static int gameState = 1; //0 = pre-game, 1 = in-game, 2 = paused game, 3 = post-game

        # region HUD textures
        Texture2D stats1;
        Texture2D powerUpMenu;
        Texture2D[] hudElements;
        #endregion

        float[] boostFade;

        //the game countdown timer
        int countDown = 5;
        //finish timer
        bool finished = false;
        int finishTimer = 180;
        int powerMenu_divisor = 1;

        //constructor
        public Gameplay(GraphicsDeviceManager gdm, TrackManager trackMan)
        {
            graphics = gdm;
            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            IsMouseVisible = false;
            trackManager = trackMan;
            animatedSprite = new AnimSpriteManager();
        }
        
        //initialising (trackManager)
        public override void Initialize()
        {
            Random randomGen = new Random();
            // obtain race settings from the gameManager inherited from GameScreen
            hPlayers = GameManager.hPlayers;
            aiPlayers = GameManager.aiPlayers;
            boostFade = new float[hPlayers];
            players = hPlayers + aiPlayers;
            // get the incoming viewport before it changes
            masterViewport = graphics.GraphicsDevice.Viewport;
            
            trackManager.DrawOrder = 1;
            Components.Add(trackManager);
            
            if (hPlayers > 1)
                powerMenu_divisor = 2;
            gameState = 1;

            //powerup lists
            mines = new List<MineDrop>();
            rockets = new List<RocketDrop>();

            //Ships
            ships = new List<Ship>();
            Ship tempShip = null;
            int model =0;
            for (int i = 0; i < players; i++)
            {
                if (i < 4)
                {
                    model = GameManager.humanModels[i];
                }
                else
                    model = randomGen.Next(9);

                tempShip = new Ship(graphics.GraphicsDevice, ContentLoader.Ship_Models[model], i+1, i+1);
                ships.Add(tempShip);
            }

            //setting up the cameras
            foreach (Ship ship in ships)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (hPlayers >= 2)
                        ship.cameras[i].aspectRatio = ((float)graphics.GraphicsDevice.Viewport.Width / (float)(Math.Ceiling((double)hPlayers / 2)) / ((graphics.GraphicsDevice.Viewport.Height / 2)));
                    else
                        ship.cameras[i].aspectRatio = ((float)graphics.GraphicsDevice.Viewport.Width / (float)(Math.Ceiling((double)hPlayers / 2)) / (graphics.GraphicsDevice.Viewport.Height));
                    ship.cameras[i].Reset();
                }
            }

            // intializing the pre-loaded models from the content loader
            coneModel = ContentLoader.Cone_Model;
            shipModel = ContentLoader.Ship_Models[GameManager.humanModels[0]];

            font1 = ContentLoader.HUD_Font[0];
            font2 = ContentLoader.HUD_Font[1];
            font3 = ContentLoader.HUD_Font[2];
            soundEngine = ContentLoader.SoundElements[2];
            //if (viewports.ToArray().Length)
        }

        //loading content
        public override void LoadContent(ContentManager content)
        {
            //loading cone textures
            coneTextures = new List<Texture2D>();
            for (int i = 0; i < 8; i++)
                coneTextures.Add(content.Load<Texture2D>("Models/Ships/CText" + (i + 1)));

            //loading the model for the checkered line
            checkeredLine = content.Load<Model>("Models/Tracks/Start");

            //HUD elements
            String[] hud = { "position_label", "position_lap", "position_hori", "boost_notification","Power-Up Menu"};
            hudElements = new Texture2D[hud.Length];
            for (int x = 0; x < hud.Length; x++ )
                hudElements[x] = content.Load<Texture2D>("UI_Elements/"+hud[x]);

            if (GameManager.hPlayers == 1)
                hudElements[4] = ContentLoader.HUD_Elements[2];
            else
                hudElements[4] = ContentLoader.HUD_Elements[3];
            
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
            
            stats1 = content.Load<Texture2D>("dialogue_box_small");
            powerUpMenu = content.Load<Texture2D>("UI_Elements/Power-Up Menu (2)");

            // load skybox
            Model b = content.Load<Model>(@"Skybox/cube");
            TextureCube a = content.Load<TextureCube>(@"Skybox/SunInSpace");
            Effect e = content.Load<Effect>(@"Skybox/Skybox");
            skybox = new Skybox(@"Skybox/SunInSpace", content, a, b, e);

            // load animated sprite textures
            if (animatedSpriteTextures == null)
            {
                int i, j = animatedSpriteFiles.GetLength(0);
                animatedSpriteTextures = new Texture2D[j];
                for (i = 0; i < j; i++)
                    animatedSpriteTextures[i] = content.Load<Texture2D>(
                            "explosions/" + animatedSpriteFiles[i]);
            }

            // load content for animated sprite manager
            Effect e2 = content.Load<Effect>(@"Shaders/AnimSprite");
            animatedSprite.LoadContent(graphics.GraphicsDevice, Content, e2);

            //setting number of players (viewports need it and load is called first)
            hPlayers = GameManager.hPlayers;
            aiPlayers = GameManager.aiPlayers;
            players = hPlayers + aiPlayers;

            //creating viewports
            viewports = new List<Viewport>();
            if (hPlayers == 1)
            {
                player1Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = 0,
                    Width = graphics.GraphicsDevice.Viewport.Width,
                    Height = graphics.GraphicsDevice.Viewport.Height,
                };
                viewports.Add(player1Viewport);
            }
            else if (hPlayers == 2)
            {
                player1Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = 0,
                    Width = graphics.GraphicsDevice.Viewport.Width,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                player2Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = graphics.GraphicsDevice.Viewport.Height / 2,
                    Width = graphics.GraphicsDevice.Viewport.Width,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                viewports.Add(player1Viewport);
                viewports.Add(player2Viewport);
            }
            else if (hPlayers >= 2)
            {
                player1Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = 0,
                    Width = graphics.GraphicsDevice.Viewport.Width / 2,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                player2Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = graphics.GraphicsDevice.Viewport.Width / 2 - 1,
                    Y = 0,
                    Width = graphics.GraphicsDevice.Viewport.Width / 2,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                player3Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = 0,
                    Y = graphics.GraphicsDevice.Viewport.Height / 2 - 1,
                    Width = graphics.GraphicsDevice.Viewport.Width / 2,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                player4Viewport = new Viewport
                {
                    MinDepth = 0,
                    MaxDepth = 1,
                    X = graphics.GraphicsDevice.Viewport.Width / 2 - 1,
                    Y = graphics.GraphicsDevice.Viewport.Height / 2 - 1,
                    Width = graphics.GraphicsDevice.Viewport.Width / 2,
                    Height = graphics.GraphicsDevice.Viewport.Height / 2,
                };

                viewports.Add(player1Viewport);
                viewports.Add(player2Viewport);
                viewports.Add(player3Viewport);
                viewports.Add(player4Viewport);
            }
        }

        protected override void UnloadContent()
        { }

        public override void RestartRace()
        {
            foreach (Ship ship in ships)
            {
                ship.Reset();
                foreach (Camera c in ship.cameras)
                    c.Reset();
            }
            finished = false;
            finishTimer = 180;
            mines.Clear();
            rockets.Clear();
            gameState = 1;
            countDown = 5;
        }
        bool musicPlaying = true;

        int currentSong = GameManager.Random4;

        //the update method
        public override void Update(GameTime gameTime)
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                if (musicPlaying)
                {
                    musicPlaying = false;
                    currentSong++;
                    if (currentSong > ContentLoader.MusicElements.Length - 1)
                        currentSong = 0;
                    MediaPlayer.Play(ContentLoader.MusicElements[currentSong]);
                }
            }
            if (MediaPlayer.State == MediaState.Playing)
            {
                musicPlaying = true;
            }

            //reseting
            if (Keyboard.GetState().IsKeyDown(Keys.R) || gamePads[0].IsButtonDown(Buttons.Back))
            {
                RestartRace();
            }

            for (int x = 0; x < boostFade.Length; x++)
            {
                boostFade[x] = ships[x].boost / 1000;
            }

            for (int x = 0; x < boostFade.Length; x++)
            {
                boostFade[x] = ships[x].boost/1000;
            }

            //updating based on game state
            //pre-game
            if (gameState == 0)
            {
                graphics.GraphicsDevice.Viewport = masterViewport;

                //moving on the next state
                if (InputState.gamePads[0].Buttons.Start == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Space))
                    gameState = 1;
                //allows the program to exit (this should not be allowed, or should be some key combination)
                if (InputState.gamePads[0].Buttons.Back == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Escape))
                    this.Exit();
            }

            //in-game
            else if (gameState == 1)
            {
                //updating the frames per second counter
                frames++;
                elapsed += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (elapsed >= 1000)
                {
                    elapsed = 0;
                    fps = frames;
                    frames = 0;
                    countDown--;
                    if (finished)
                        finishTimer--;
                }

                //updating the finished status
                int playersFinished = 0;
                foreach (Ship ship in ships)
                {
                    if (ship.finished)
                    {
                        if (ship.player <= 4)
                            playersFinished++;
                        finished = true;
                        if (GameManager.Positioning.IndexOf(ship) == -1)
                        {
                            GameManager.Positioning.Add(ship);
                        }
                    }
                }
                if (GameManager.Positioning.ToArray().Length == players)
                {
                    gameState = 3;
                }
                if (playersFinished == players)
                {
                    //uncomment to collect waypoints
                    //ships[0].sw.Close();
                    //ships[0].fs.Close();
                    // screenMan.endGame(this, 2);
                    gameState = 3; //need some post-game screen to come in here
                }

                //updating the activated powerup collections
                foreach (Ship ship in ships)
                {
                    if (ship.mine != null)
                        mines.Add(ship.mine);
                    ship.mine = null;
                    if (ship.rocket != null)
                        rockets.Add(ship.rocket);
                    ship.rocket = null;
                }

                //updating the powerups in the activated collections
                for (int i = 0; i < mines.Count(); i++)
                {
                    mines[i].lifetime -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (mines[i].lifetime <= 0)
                        mines.RemoveAt(i);
                }
                for (int i = 0; i < rockets.Count(); i++)
                {
                    rockets[i].Update(gameTime.ElapsedGameTime.TotalMilliseconds);
                    rockets[i].lifetime -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (rockets[i].lifetime <= 0)
                        rockets.RemoveAt(i);
                }

                // update animated sprites
                animatedSprite.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

                //update the ship
                for (int i = 0; i < ships.Count(); i++)
                {
                    ships[i].laps = GameManager.Laps;
                    ships[i].lapLength = trackManager.trackLength;
                    if (ships[i].agent == null)
                        ships[i].Update(gameTime, gamePads);
                    else
                    {
                        ships[i].agent.piece1 = trackManager.findPiece(ships[i].trackNumber);
                        ships[i].agent.piece2 = trackManager.findPiece(ships[i].trackNumber + 1);
                        ships[i].agent.piece3 = trackManager.findPiece(ships[i].trackNumber + 2);
                        ships[i].Update(gameTime, gamePads);
                    }
                    checkCollisions(ships[i], i);
                    if (countDown <= 0)
                        ships[i].countingDown = false;
                    trackMan.Update(gameTime);
                }

                //updating the race ladder
                ladder = new List<float>();
                //cloning to check and change without affecting originals
                List<Ship> temp = new List<Ship>();
                foreach (Ship ship in ships)
                    temp.Add(ship);
                while (temp.Count() > 0)
                {
                    float max = 0;
                    int index = 0;
                    for (int i = 0; i < temp.Count(); i++)
                    {
                        if (temp[i].distance > max)
                        {
                            max = temp[i].distance;
                            index = i;
                        }
                    }
                    ladder.Add(temp[index].player);
                    temp.Remove(temp[index]);
                }

                //updating split times
                for (int i = 0; i < ships.Count; i++)
                {
                    if (Vector3.Distance(ships[i].position, trackManager.checkpoints[ships[i].currentCheckpoint]) < 20)
                    {
                        if (GameManager.TrackSelect == 1)
                        {
                            ships[i].currentCheckpoint++;
                            if (ships[i].currentCheckpoint >= trackManager.checkpoints.Count())
                                ships[i].currentCheckpoint = 0;
                            if (ships[i].currentCheckpoint != 0)
                            {
                                if (trackManager.splittimes[ships[i].currentCheckpoint - 1] != 0)
                                    ships[i].splitTime = trackManager.splittimes[ships[i].currentCheckpoint - 1] - ships[i].lapTime;
                                else
                                {
                                    trackManager.splittimes[ships[i].currentCheckpoint - 1] = ships[i].lapTime;
                                    ships[i].splitTime = 0;
                                }
                            }
                            else
                            {
                                if (trackManager.splittimes[trackManager.splittimes.Count() - 1] != 0)
                                    ships[i].splitTime = trackManager.splittimes[trackManager.splittimes.Count() - 1] - ships[i].lapTime;
                                else
                                {
                                    trackManager.splittimes[trackManager.splittimes.Count() - 1] = ships[i].lapTime;
                                    ships[i].splitTime = 0;
                                }
                            }
                        }
                        //track 0 needs to only update at lap start
                        else
                        {
                            ships[i].currentCheckpoint++;
                            if (ships[i].currentCheckpoint >= trackManager.checkpoints.Count())
                                ships[i].currentCheckpoint = 0;
                            if (ships[i].currentCheckpoint != 0)
                            {
                                if (trackManager.splittimes[ships[i].currentCheckpoint - 1] != 0)
                                    ships[i].splitTime = trackManager.splittimes[ships[i].currentCheckpoint - 1] - ships[i].lapTime;
                                else
                                {
                                    trackManager.splittimes[ships[i].currentCheckpoint - 1] = ships[i].lapTime;
                                    ships[i].splitTime = 0;
                                }
                            }
                        }
                    }
                }

                //pausing the game - moving to state 2
                if (InputState.gamePads[0].Buttons.Start == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Escape))
                {
                    graphics.GraphicsDevice.Viewport = masterViewport;
                    this.screenMan.changeScreen(this, 1, true);
                }
                

                //showing the optional HUD
                if (InputState.keyboard.IsKeyDown(Keys.F3))
                    showStats = true;
                if (InputState.keyboard.IsKeyDown(Keys.F4))
                    showStats = false;
            }

            //game paused
            else if (gameState == 2)
            {
                graphics.GraphicsDevice.Viewport = masterViewport;
                //unpausing - moving back to state 1
                if (InputState.gamePads[0].Buttons.Back == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Escape))
                {
                    gameState = 1;
                }
                //should be an option to exit to state 0, (or 3, depends on how we handle it)
                //moving on the next state
                if (InputState.gamePads[0].Buttons.A == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Enter))
                {
                    gameState = 3;
                }
            }

            //post-game
            else 
            {
                // will have to change to results screen - pass the result values to GameManager (placings and times)
                //screenMan.changeScreen(this,2,false);
                screenMan.endGame(this, 2);

                //sending players back to state 0
                if (InputState.gamePads[0].Buttons.B == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Escape)
                    || InputState.gamePads[0].Buttons.Start == ButtonState.Pressed || InputState.keyboard.IsKeyDown(Keys.Space))
                {
                    gameState = 0;
                }
            }
        }

        //checking for collisions - should be done for each ship
        private void checkCollisions(Ship ship, int shipNUmber)
        {
            //*********************** checking against other ships **********************************

            BoundingSphere shipSphere = new BoundingSphere();
            shipSphere.Center = ship.position;
            shipSphere.Radius = 2.5f;

            for (int s = shipNUmber+1; s < ships.Count; s++)
            {
                if (ships[s].player == ship.player) { }
                else
                {
                    BoundingSphere sphere2 = new BoundingSphere();
                    sphere2.Center = ships[s].position;
                    sphere2.Radius = 3;

                    //slows down the player that is behind
                    if (shipSphere.Intersects(sphere2))
                    {
                        if (ship.lap > ships[s].lap)
                            ship.speed -= 5;
                        else if (ladder.IndexOf(ship.player) < ladder.IndexOf(ships[s].player))
                            ships[s].speed -= 5;
                        else
                            ship.speed -= 5;
                    }
                }
            }

            //*********************** checking against power-ups *********************************

            //checking for collision with powerups
            foreach (Powerup p in trackMan.getPowerups())
            {
                BoundingSphere sphere2 = new BoundingSphere();
                sphere2.Center = p.position;
                sphere2.Radius = 2.5f;

                if (shipSphere.Intersects(sphere2) && p.acquired == false)
                {
                    if (ship.power1 == null)
                    {
                        ship.power1 = p.acquire();
                        soundEngine.Play();
                        // add powerup spawn animates sprite
                        AddAnimSprite(AnimSpriteType.Spawn, p.position, 1, 40, 30, DrawMode.Additive, -1);
                    }
                    else if (ship.power2 == null)
                    {
                        ship.power2 = p.acquire();
                        soundEngine.Play();
                        // add powerup spawn animates sprite
                        AddAnimSprite(AnimSpriteType.Spawn, p.position, 1, 40, 30, DrawMode.Additive, -1);
                    }
                }
            }

            //colliding with activated powerups
            for (int i = 0; i < mines.Count(); i++)
            {
                BoundingSphere sphere2 = new BoundingSphere();
                sphere2.Center = mines[i].position;
                sphere2.Radius = 2.5f;

                if (shipSphere.Intersects(sphere2))
                {
                    AddAnimSprite(AnimSpriteType.Spawn, mines[i].position, 10, 40, 30, DrawMode.Additive, -1);
                    ship.speed = ship.speed / 2;
                    mines.RemoveAt(i);
                }
            }
            for (int i = 0; i < rockets.Count(); i++)
            {
                BoundingSphere sphere2 = new BoundingSphere();
                sphere2.Center = rockets[i].position;
                sphere2.Radius = 2.5f;

                if (shipSphere.Intersects(sphere2))
                {
                    AddAnimSprite(AnimSpriteType.Spawn, rockets[i].position, 10, 40, 30, DrawMode.Additive, -1);
                    ship.speed = ship.speed / 2;
                    rockets.RemoveAt(i);
                }
            }

            //checking powerup collisions
            List<int> minesToRemove = new List<int>();
            List<int> rocketsToRemove = new List<int>();
            for (int i = 0; i < mines.Count(); i++)
            {
                BoundingSphere sphere1 = new BoundingSphere();
                sphere1.Center = mines[i].position;
                sphere1.Radius = 2.5f;
                for (int j = i; j < rockets.Count(); j++)
                {
                    BoundingSphere sphere2 = new BoundingSphere();
                    sphere2.Center = rockets[j].position;
                    sphere2.Radius = 2.5f;

                    if (sphere1.Intersects(sphere2))
                    {
                        minesToRemove.Add(i);
                        rocketsToRemove.Add(j);
                    }
                }
            }
            for (int i = 0; i < minesToRemove.Count(); i++)
                mines.RemoveAt(i);
            for (int i = 0; i < rocketsToRemove.Count(); i++)
                rockets.RemoveAt(i);

            //*********************** checking against track pieces *********************************

            List<TrackPiece> track = trackManager.SurroundingTrackPieces(ship.trackNumber);// - must make this work and all will be good (just have to change a few things)
            foreach (TrackPiece TP in track)
            {
                //check if ship is within track piece's boundaries (sphere is inaccurate)
                Matrix rotation = Matrix.CreateRotationY(-TP.yaw) * Matrix.CreateRotationX(-TP.pitch);
                Vector3 shipPosition = ship.position - TP.position;
                shipPosition = Vector3.Transform(shipPosition, rotation);

                if ((shipPosition.X < TP.bottomRight.X && shipPosition.X > TP.topLeft.X) && (shipPosition.Z < TP.bottomRight.Z && shipPosition.Z > TP.topLeft.Z))
                {
                    //stuff that happens for all pieces
                    ship.position.Y = TP.getHeight(ship.position) + ship.altitude;
                    ship.trackUp = Vector3.TransformNormal(Vector3.Up, TP.rotationMatrix);
                    ship.trackNumber = TP.pieceNumber;

                    //updating ship distance for ranking purposes
                    float shipDist = Vector3.Distance(ship.position, track[0].position) - track[0].bottomRight.Z;
                    float percentage = shipDist / (track[1].bottomRight.Z * 2);
                    ship.distance = (ship.lap - 1) * ship.lapLength + ship.trackNumber + percentage;

                    //for the straights
                    if (TP.trackType == 1 || TP.trackType == 2)
                    {
                        ship.currentPiece = TP;
                        ship.nextPiece = trackManager.findPiece(TP.pieceNumber + 1);
                        return;
                    }

                    //90 degree corner
                    else if (TP.trackType == 3)
                    {
                        if (((shipPosition.X < (TP.bottomRight.X - 40)) && (shipPosition.Z > (TP.bottomRight.Z - 7)))
                            || ((shipPosition.X < (TP.bottomRight.X - 43)) && (shipPosition.Z > (TP.bottomRight.Z - 13)))
                            || ((shipPosition.X < (TP.bottomRight.X - 48.2f)) && (shipPosition.Z > (TP.bottomRight.Z - 18)))
                            || ((shipPosition.X < (TP.bottomRight.X - 54.5f)) && (shipPosition.Z > (TP.bottomRight.Z - 21)))
                            || ((shipPosition.X > (TP.topLeft.X + 10f)) && (shipPosition.Z < (TP.topLeft.Z + 1.1f)))
                            || ((shipPosition.X > (TP.topLeft.X + 19.5f)) && (shipPosition.Z < (TP.topLeft.Z + 2.5f)))
                            || ((shipPosition.X > (TP.topLeft.X + 27.5f)) && (shipPosition.Z < (TP.topLeft.Z + 6.5f)))
                            || ((shipPosition.X > (TP.topLeft.X + 35.1f)) && (shipPosition.Z < (TP.topLeft.Z + 10.1f)))
                            || ((shipPosition.X > (TP.topLeft.X + 42.1f)) && (shipPosition.Z < (TP.topLeft.Z + 16.8f)))
                            || ((shipPosition.X > (TP.topLeft.X + 49.3f)) && (shipPosition.Z < (TP.topLeft.Z + 22.7f)))
                            || ((shipPosition.X > (TP.topLeft.X + 55.7f)) && (shipPosition.Z < (TP.topLeft.Z + 35.7f))))
                        {
                            if (!ship.finished)
                            {
                                if (TP.pieceNumber == trackManager.trackLength)
                                    ship.Respawn(TP.position, trackManager.findPiece(1).position, TP.yaw, TP.trackType, TP.world);
                                else
                                    ship.Respawn(TP.position, trackManager.findPiece(TP.pieceNumber + 1).position, TP.yaw, TP.trackType, TP.world);
                            }
                            return;
                        }
                        else
                        {
                            ship.currentPiece = TP;
                            ship.nextPiece = trackManager.findPiece(TP.pieceNumber + 1);
                            return;
                        }
                    }

                    //45 degree corner
                    else if (TP.trackType == 4)
                    {
                        if (((shipPosition.X < TP.bottomRight.X - 41) && (shipPosition.Z > TP.bottomRight.Z - 6.9f))
                            || ((shipPosition.X < TP.bottomRight.X - 44.3f) && (shipPosition.Z > TP.bottomRight.Z - 14.5f))
                            || ((shipPosition.X < TP.bottomRight.X - 47.8f) && (shipPosition.Z > TP.bottomRight.Z - 20.1f))
                            || ((shipPosition.X < TP.bottomRight.X - 50.4f) && (shipPosition.Z > TP.bottomRight.Z - 25.8f))
                            || ((shipPosition.X > TP.topLeft.X + 23.5f) && (shipPosition.Z < TP.topLeft.Z + 3))
                            || ((shipPosition.X > TP.topLeft.X + 31) && (shipPosition.Z < TP.topLeft.Z + 5))
                            || ((shipPosition.X > TP.topLeft.X + 37.1f) && (shipPosition.Z < TP.topLeft.Z + 8.5f))
                            || ((shipPosition.X > TP.topLeft.X + 43) && (shipPosition.Z < TP.topLeft.Z + 10.8f))
                            || ((shipPosition.X > TP.topLeft.X + 45.8) && (shipPosition.Z < TP.topLeft.Z + 21.5f))
                            || ((shipPosition.X > TP.topLeft.X + 47) && (shipPosition.Z < TP.topLeft.Z + 28.4f))
                            || ((shipPosition.X > TP.topLeft.X + 56) && (shipPosition.Z < TP.topLeft.Z + 44.4f)))
                        {
                            if (!ship.finished)
                            {
                                if (TP.pieceNumber == trackManager.trackLength)
                                    ship.Respawn(TP.position, trackManager.findPiece(1).position, TP.yaw, TP.trackType, TP.world);
                                else
                                    ship.Respawn(TP.position, trackManager.findPiece(TP.pieceNumber + 1).position, TP.yaw, TP.trackType, TP.world);
                            }
                            return;
                        }
                        else
                        {
                            ship.currentPiece = TP;
                            ship.nextPiece = trackManager.findPiece(TP.pieceNumber + 1);
                            return;
                        }
                    }
                }
            }
            //making the ship respawn if off track
            if (!ship.finished)
            {
                if (trackManager.findPiece(ship.trackNumber).pieceNumber == trackManager.trackLength)
                    ship.Respawn(trackManager.findPiece(ship.trackNumber).position, trackManager.findPiece(1).position, trackManager.findPiece(ship.trackNumber).yaw, trackManager.findPiece(ship.trackNumber).trackType, trackManager.findPiece(ship.trackNumber).world);
                else
                    ship.Respawn(trackManager.findPiece(ship.trackNumber).position, trackManager.findPiece(ship.trackNumber + 1).position, trackManager.findPiece(ship.trackNumber).yaw, trackManager.findPiece(ship.trackNumber).trackType, trackManager.findPiece(ship.trackNumber).world);
            }
        }

        //drawing the game assets
        public override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(new Color(0.02f, 0.02f, 0.06f));

            graphics.GraphicsDevice.BlendState = BlendState.Opaque;
            graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            graphics.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            //changing what is drawn based on game state
            //pre-game
            if (gameState == 0)
            {
                spriteBatch.Begin();
                //message
                string message = "PRE GAME \n PRESS START";
                spriteBatch.DrawString(font1, message, new Vector2((float)graphics.GraphicsDevice.Viewport.Width / 2 - 20, (float)graphics.GraphicsDevice.Viewport.Height / 2), Color.White);
                spriteBatch.End();
            }

            //in-game
            else if (gameState == 1 || gameState == 2)
            {
                //drawing the ships - should loop through each viewport not each ship - AI don't have cameras
                for (int i = 0; i < hPlayers; i++)
                {
                    graphics.GraphicsDevice.Viewport = viewports[i];
                    Camera camera = ships[i].cameras[ships[i].currentCamera];

                    // draw skybox
                    RasterizerState originalRasterizerState = graphics.GraphicsDevice.RasterizerState;
                    RasterizerState rasterizerState = new RasterizerState();
                    rasterizerState.CullMode = CullMode.None;
                    graphics.GraphicsDevice.RasterizerState = rasterizerState;
                    skybox.Draw(camera.View, camera.Projection, ships[i].position);
                    graphics.GraphicsDevice.RasterizerState = originalRasterizerState;

                    DrawModel(ships[i].model, ships[i].world, camera);

                    //track
                    trackManager.DrawTrack(camera, ships[i].trackNumber);

                    // draw animated sprites
                    animatedSprite.Draw(graphics.GraphicsDevice, camera.Position(), ships[i].up,
                        (camera.View * camera.Projection), 0, false);

                    //powerups
                    foreach (MineDrop md in mines)
                        md.DrawP(camera);
                    foreach (RocketDrop rd in rockets)
                        rd.DrawP(camera);

                    //start line
                    if (Vector3.Distance(ships[i].position, new Vector3(0, 0.5f, -5)) < 150)
                    {
                        Matrix startWorld = Matrix.Identity;
                        startWorld.Translation = new Vector3(0, 0.5f, -5);
                        DrawModel(checkeredLine, startWorld, camera);
                    }

                    //drawing the other ships
                    for (int j = 0; j < ships.Count(); j++)
                    {
                        if (i != j)
                        {
                            DrawModel(ships[j].model, ships[j].world, camera);
                            //drawing a cone over the other ships
                            Matrix coneWorld = ships[j].world;
                            coneWorld.Translation = ships[j].world.Translation + new Vector3(0, 2.8f, 0);
                            DrawCone(coneModel, coneWorld, camera, coneTextures[ships[j].player - 1]);
                        }
                    }
                }

                for (int i = 0; i < hPlayers; i++)
                {
                    graphics.GraphicsDevice.Viewport = viewports[i];
                    DrawHUD(i);
                }
                graphics.GraphicsDevice.Viewport = masterViewport;

                if (gameState == 2)
                {
                    spriteBatch.Begin();
                    //message
                    string message = "PAUSED \n PRESS BACK";
                    spriteBatch.DrawString(font1, message, new Vector2((float)graphics.GraphicsDevice.Viewport.Width / 2 - 20, (float)graphics.GraphicsDevice.Viewport.Height / 2), Color.White);
                    spriteBatch.End();
                }
            }

            //post-game
            else
            {
                spriteBatch.Begin();
                //message
                string message = "POST GAME \n PRESS START OR BACK";
                spriteBatch.DrawString(font1, message, new Vector2((float)graphics.GraphicsDevice.Viewport.Width / 2 - 20, (float)graphics.GraphicsDevice.Viewport.Height / 2), Color.White);
                spriteBatch.End();
            }
        }

        /// <summary>
        /// Create a new animated sprite and add it to the animated sprite manager
        /// </summary>
        public AnimSprite AddAnimSprite(
            AnimSpriteType type,
            Vector3 position,
            float radius,
            float viewOffset,
            float frameRate,
            DrawMode mode,
            int player)
        {
            // create animated sprite
            AnimSprite a = new AnimSprite(type, position, radius, viewOffset,
                animatedSpriteTextures[(int)type], 256, 256,
                frameRate, mode, player);

            // add it to the animated sprite manager
            animatedSprite.Add(a);

            return a;
        }

        //sepate draw method for cones
        private void DrawCone(Model model, Matrix world, Camera camera, Texture2D texture)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                    //Texture
                    effect.Texture = texture;
                    effect.TextureEnabled = true;

                    //lighting
                    effect.DirectionalLight0.DiffuseColor = new Vector3(1f, 1f, 1f);
                    effect.DirectionalLight0.Direction = new Vector3(0, 2, 0);
                    effect.DirectionalLight0.Enabled = true;

                    effect.AmbientLightColor = new Vector3(1f, 1f, 1f);
                    effect.LightingEnabled = true;
                }
                mesh.Draw();
            }
        }

        private bool checkIfShip(Model model)
        {

            for (int x = 0; x < ContentLoader.Ship_Models.Length; x++)
            {
                if (ContentLoader.Ship_Models[x] == model)
                    return true;
            }

                return false;
        }

        //seperate draw method for drawing models
        private void DrawModel(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                    // need to check if model is a ship
                    if (checkIfShip(model))
                    {
                        //lighting
                        effect.DirectionalLight0.DiffuseColor = new Vector3(0.3f, 0.3f, 0.1f);
                        effect.DirectionalLight0.Direction = new Vector3(5, -10, 4);
                        effect.DirectionalLight0.Enabled = true;
                        effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                        effect.DirectionalLight1.DiffuseColor = new Vector3(0, 0, 1);
                        effect.DirectionalLight1.Direction = new Vector3(0, 0, -5);
                        effect.DirectionalLight1.Enabled = true;

                        effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                        effect.LightingEnabled = true;
                    }
                }
                mesh.Draw();
            }
        }

        //drawing the HUD, in this case testing text
        private void DrawHUD(int shipID)
        {
            spriteBatch.Begin();
            int width = graphics.GraphicsDevice.Viewport.Width;
            int height = graphics.GraphicsDevice.Viewport.Height;

            //drawing the countdown
            if (countDown >= 0)
            {
                string count;
                if (countDown == 0)
                    count = "GO!";
                else
                    count = "" + countDown;
                spriteBatch.Draw(ContentLoader.HUD_Elements[12 + countDown], new Vector2(width / 2 - ContentLoader.HUD_Elements[12 + countDown].Width / 2,
                    height / 2 - ContentLoader.HUD_Elements[12 + countDown].Height / 2), Color.White);
            }
            //drawing the finished stats
            if (ships[shipID].finished == true)
            {
                string finish = "Time: " + ships[shipID].totalTime + "\n" + "Best Lap: " + ships[shipID].bestLap;
                //spriteBatch.Draw(stats1, new Rectangle(width/2 - stats1.Width,height - stats1.Height,width/2,height/2), Color.LightGreen);
                spriteBatch.DrawString(font3, "FINISHED", new Vector2((width / 2) - 90, height / 2 - 50), Color.LightBlue);
                spriteBatch.DrawString(font1, finish, new Vector2((width / 2) - 90, height / 2), Color.White);
            
            }
            //showing the optional testing stats
            if (showStats)
            {
                //speed
                string speed = "Speed: " + ships[shipID].speed;
                spriteBatch.DrawString(font1, speed, new Vector2(10, 130), Color.LightGreen);
                //gear
                string gear = "Gear: " + ships[shipID].gear;
                spriteBatch.DrawString(font1, gear, new Vector2(10, 150), Color.LightGreen);
                //position
                string position = "Position: " + ships[shipID].position.X + " " + ships[shipID].position.Y + " " + ships[shipID].position.Z;
                spriteBatch.DrawString(font1, position, new Vector2(10, 170), Color.LightGreen);
                //boost
                string boost = "Boost: " + ships[shipID].boost;
                spriteBatch.DrawString(font1, boost, new Vector2(10, 190), Color.LightGreen);
                //force
                string force = "Acc: " + ships[shipID].accelerationForce;
                spriteBatch.DrawString(font1, force, new Vector2(10, 210), Color.LightGreen);
                //track piece
                string track = "Track: " + trackManager.SurroundingTrackPieces(ships[shipID].trackNumber)[0].pieceNumber + " " + trackManager.SurroundingTrackPieces(ships[shipID].trackNumber)[1].pieceNumber + " " + trackManager.SurroundingTrackPieces(ships[shipID].trackNumber)[2].pieceNumber;
                spriteBatch.DrawString(font1, track, new Vector2(10, 230), Color.LightGreen);
                //frames per second
                string framesPerSec = "FPS: " + fps;
                spriteBatch.DrawString(font1, framesPerSec, new Vector2(graphics.GraphicsDevice.Viewport.Width - 80, 70), Color.LightGreen);
                //current lap
                string lap = "Lap: " + ships[shipID].lap;
                spriteBatch.DrawString(font1, lap, new Vector2(10, 250), Color.LightGreen);
                //current lap
                string time = "Time: " + Math.Floor(ships[shipID].lapTime / 60) + ":" + Math.Floor(ships[shipID].lapTime % 60) + ":" + Math.Round(ships[shipID].lapTime % 60 % 1, 2) * 100;
                spriteBatch.DrawString(font1, time, new Vector2(10, 270), Color.LightGreen);
                //current lap
                string best = "Best: " + Math.Floor(ships[shipID].bestLap / 60) + ":" + Math.Floor(ships[shipID].bestLap % 60) + ":" + Math.Round(ships[shipID].bestLap % 60 % 1, 2) * 100;
                spriteBatch.DrawString(font1, best, new Vector2(10, 290), Color.LightGreen);
                //percentage
                string percent = "Distance: " + ships[shipID].distance;
                spriteBatch.DrawString(font1, percent, new Vector2(10, 310), Color.LightGreen);

                if (!ships[shipID].finished && finishTimer <= 20)
                {
                    string countdown = "" + finishTimer;
                    spriteBatch.DrawString(font1, countdown, new Vector2(graphics.GraphicsDevice.Viewport.Width / 2, 100), Color.LightGreen);
                }
                //split time
                string positioning = "";
                if (ships[shipID].splitTime >= 0)
                    positioning = "Ahead";

                string split = "Split: " + ships[shipID].splitTime;
                spriteBatch.DrawString(font1, split, new Vector2(width - 200, 75), Color.White);
                //spriteBatch.DrawString(font1, "CheckPoint: "+ships[shipID].currentCheckpoint, new Vector2(screenWidth - 200, 150), Color.White);
            }    

            //ladder ranking
            string rank = "";
            for (int i = 0; i < ladder.Count(); i++)
                if (ships[shipID].player == ladder[i])
                {
                    rank += (i + 1);
                    ships[shipID].ranking = ladder[i];
                    break;
                }

            Color alpha = new Color(boostFade[shipID] * 20, boostFade[shipID] * 20, boostFade[shipID] * 20, boostFade[shipID]);
            // power-up menu
            //powerups
            string powerup1 = "P1: empty";
            string powerup2 = "P2: empty";
            if (ships[shipID].power1 != null)
            {
                if (ships[shipID].power1.type == 6)
                    powerup1 = "P1: Mine";
                else
                    powerup1 = "P1: Rocket";
            }
            if (ships[shipID].power2 != null)
            {
                if (ships[shipID].power2.type == 6)
                    powerup2 = "P2: Mine";
                else
                    powerup2 = "P2: Rocket";
            }

            int powerWidth = 35;
            int powerHeight = height - hudElements[4].Width / powerMenu_divisor - 50; ;

            int menu = 3;

            if (powerup2.Equals("P2: Rocket") && powerup1.Equals("P1: Mine"))
                menu = 5;//full2
            else if (powerup2.Equals("P2: Rocket") && powerup1.Equals("P1: empty"))
                menu = 9;//rocket2
            else if (powerup2.Equals("P2: empty") && powerup1.Equals("P1: Mine"))
                menu = 7;//mine2
            else if (powerup1.Equals("P1: Rocket") && powerup2.Equals("P2: Mine"))
                menu = 4;//full1
            else if (powerup1.Equals("P1: Rocket") && powerup2.Equals("P2: empty") )
                menu = 8;//rocket1
            else if (powerup1.Equals("P1: empty") && powerup2.Equals("P2: Mine") )
                menu = 6;//mine1
            else if (powerup1.Equals("P1: Rocket") && powerup2.Equals("P2: Rocket"))
                menu = 11;//mine1
            else if (powerup1.Equals("P1: Mine") && powerup2.Equals("P2: Mine"))
                menu = 10;//mine1
            
            



            spriteBatch.Draw(ContentLoader.HUD_Elements[menu], new Rectangle(powerWidth,powerHeight,
                ContentLoader.HUD_Elements[menu].Width / powerMenu_divisor, ContentLoader.HUD_Elements[menu].Height / powerMenu_divisor), Color.White);
            
            // boost guage
          //  spriteBatch.DrawString(font2, "BOOST", new Vector2(width - 240, height - 53),
          //      new Color(boostFade[shipID] * 20, boostFade[shipID] * 20, boostFade[shipID] * 20, boostFade[shipID]));    
            int boostbarW = width - 300;
            int boostbarH = height - 45;
            Texture2D boostbar = ContentLoader.HUD_Elements[1];
            spriteBatch.Draw(boostbar, new Rectangle(boostbarW+50, boostbarH-2, ContentLoader.HUD_Elements[1].Width, ContentLoader.HUD_Elements[1].Height), alpha);
            spriteBatch.Draw(stats1, new Rectangle(boostbarW, boostbarH, (int)(ships[shipID].boost * 2.5), 30), Color.White);
            spriteBatch.DrawString(font2, ""+(int)ships[shipID].speed+" pc/h", new Vector2(width-250, height - 100), Color.White);


            string time2 = "Time: " + Math.Floor(ships[shipID].lapTime / 60) + ":" + Math.Floor(ships[shipID].lapTime % 60) + ":" + Math.Round(ships[shipID].lapTime % 60 % 1, 2) * 100;
            spriteBatch.DrawString(font1, time2, new Vector2(50, 70), Color.White);
            string best2 = "Best: " + Math.Floor(ships[shipID].bestLap / 60) + ":" + Math.Floor(ships[shipID].bestLap % 60) + ":" + Math.Round(ships[shipID].bestLap % 60 % 1, 2) * 100;
            spriteBatch.DrawString(font1, best2, new Vector2(50, 100), Color.White);

            int current_lap = ships[shipID].lap;
            if (current_lap > GameManager.Laps)
                current_lap = GameManager.Laps;
            spriteBatch.DrawString(font2, current_lap + "/" + GameManager.Laps, new Vector2(150, 18), Color.White);
            spriteBatch.DrawString(font2, rank + "/" + (aiPlayers + hPlayers), new Vector2(width - 100, 18), Color.White);

            spriteBatch.Draw(hudElements[2], new Rectangle(0, 10, width, 50), Color.White);

            spriteBatch.Draw(hudElements[1], new Vector2(25, 10), Color.White);
            spriteBatch.Draw(hudElements[0], new Vector2(width - 200, 10), Color.White);

            spriteBatch.End();
        }
    }
}
