﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ISR
{
    //the base camera class
    public class Camera
    {
        #region Matrices

        //view transform matrix
        public Matrix View;

        //projection matrix
        public Matrix Projection;

        //Reset function
        public virtual void Reset() { }

        // get position
        public virtual Vector3 Position()
        {
            return Vector3.One;
        }

        #endregion

        //perspective properties
        public float aspectRatio = 4.0f / 3.0f;
    }
}
