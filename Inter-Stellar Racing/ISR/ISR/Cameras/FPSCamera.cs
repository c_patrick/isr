﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ISR
{
    class FPSCamera : Camera
    {
        #region Properties
        //target properties
        Vector3 desiredPositionOffset = new Vector3(0, 0.79f, -1f);
        public Vector3 targetPosition;
        public Vector3 targetDirection;
        public Vector3 up = Vector3.Up;
        Vector3 lookAt;
        Vector3 position;

        //perspective
        public float fieldOfView = MathHelper.ToRadians(45);
        public float nearPlaneDistance = 0.001f;
        public float farPlaneDistance = 10000;
        #endregion

        #region Position and Matrice Updates
        //updating the world position
        private void UpdateWorldPositions()
        {
            //construct transformation matrix for world space
            Matrix transform = Matrix.Identity;
            transform.Forward = targetDirection;
            transform.Up = up;
            transform.Right = Vector3.Cross(up, targetDirection);

            //desired camera properties in world space
            targetPosition = targetPosition + Vector3.TransformNormal(desiredPositionOffset, transform);
            lookAt = targetPosition + (targetDirection * 20);
        }

        //updating the matrices
        private void UpdateMatrices()
        {
            //rebuild the view and projection matrices
            View = Matrix.CreateLookAt(this.position, this.lookAt, this.up);
            Projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance);
        }
        #endregion

        #region Reset
        //reseting the camera
        public override void Reset()
        {
            UpdateWorldPositions();
            position = targetPosition + new Vector3(0, 2, -1);
            UpdateMatrices();
        }
        #endregion

        public override Vector3 Position()
        {
            return position;
        }

        #region Update Code
        //updating the camera
        public void Update(GameTime gameTime)
        {
            UpdateWorldPositions();
            position = targetPosition;
            UpdateMatrices();
        }
        #endregion
    }
}
