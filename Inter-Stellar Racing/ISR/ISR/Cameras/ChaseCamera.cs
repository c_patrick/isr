﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ISR
{
    //third-person perspective
    class ChaseCamera : Camera
    {
        #region Properties
        //target properties
        public Vector3 targetPosition;
        public Vector3 targetDirection;
        public Vector3 up = Vector3.Up;

        //desired position
        Vector3 desiredPositionOffset = new Vector3(0, 4, 8);
        Vector3 desiredPosition;
        Vector3 lookAt;

        //camera physics
        float stiffness = 7000;
        float damping = 600;
        float mass = 50;

        //current camera properties
        Vector3 position;
        Vector3 velocity;

        //perspective
        public float fieldOfView = MathHelper.ToRadians(45);
        public float nearPlaneDistance = 1;
        public float farPlaneDistance = 10000;
        #endregion

        #region Position and Matrice Updates
        //updating the world position
        private void UpdateWorldPositions()
        {
            //construct transformation matrix for world space
            Matrix transform = Matrix.Identity;
            transform.Forward = targetDirection;
            transform.Up = up;
            transform.Right = Vector3.Cross(up, targetDirection);

            //desired camera properties in world space
            desiredPosition = targetPosition + Vector3.TransformNormal(desiredPositionOffset, transform);
            lookAt = targetPosition;
        }

        //updating the matrices
        private void UpdateMatrices()
        {
            //rebuild the view and projection matrices
            View = Matrix.CreateLookAt(this.position, this.lookAt, this.up);
            Projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance);
        }
        #endregion

        #region Reset
        //reseting the camera
        public override void Reset()
        {
            UpdateWorldPositions();
            //stopping motion
            velocity = Vector3.Zero;
            //teleport
            position = desiredPosition;
            UpdateMatrices();
        }
        #endregion

        public override Vector3 Position()
        {
            return position;
        }

        #region Update Code
        //updating the camera
        public void Update(GameTime gameTime)
        {
            UpdateWorldPositions();

            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;
            //spring force
            Vector3 stretch = position - desiredPosition;
            Vector3 force = -stiffness * stretch - damping * velocity;
            //accelerating
            Vector3 acceleration = force / mass;
            velocity += acceleration * time;
            //moving
            position += velocity * time;

            UpdateMatrices();
        }
        #endregion
    }
}
