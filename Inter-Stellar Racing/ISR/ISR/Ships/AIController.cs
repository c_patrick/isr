﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.IO;

namespace ISR
{
    class AIController
    {
        public TrackPiece piece1;
        public TrackPiece piece2;
        public TrackPiece piece3;

        public List<Vector3> waypoints;
        public List<float> speeds;
        public int targetIndex;
        public float speed;

        bool boosting;

        public AIController()
        {
            waypoints = new List<Vector3>();
            speeds = new List<float>();

            float difficulty = GameManager.AIdifficulty;
            string filename = "track" + GameManager.TrackSelect + "waypoints" + ((difficulty * 4) + GameManager.Random4) + ".txt";

            string line;
            StreamReader sr = new StreamReader("Content/Tracks/" + filename);
            while ((line = sr.ReadLine()) != null)
            {
                string[] points = line.Split();
                double x;
                if (!Double.TryParse(points[0], out x))
                {
                    string num = points[0];
                    num = num.Replace(".", ",");
                    x = (float)Double.Parse(num);
                }
                double y;
                if (!Double.TryParse(points[1], out y))
                {
                    string num = points[1];
                    num = num.Replace(".", ",");
                    y = (float)Double.Parse(num);
                }
                double z;
                if (!Double.TryParse(points[2], out z))
                {
                    string num = points[2];
                    num = num.Replace(".", ",");
                    x = (float)Double.Parse(num);
                }
                Vector3 position = new Vector3((float)x, (float)y, (float)z);
                waypoints.Add(position);
                double speedD;
                if (!Double.TryParse(points[3], out speedD))
                {
                    string num = points[3];
                    num = num.Replace(".", ",");
                    speedD = (float)Double.Parse(num);
                }
                speed = (float)speedD;
                speeds.Add(speed);
            }
            sr.Close();
            targetIndex = 10;
        }

        public void Update(Ship ship)
        {
            boosting = false;

            //getting the correct waypoint to aim at
            bool notChanged = true;
            while (notChanged)
            {
                if (targetIndex >= waypoints.Count())
                    targetIndex -= waypoints.Count();
                if (Math.Abs(Vector3.Distance(waypoints[targetIndex], ship.position)) < 3)
                    targetIndex++;
                else
                    notChanged = false;
            }

            Vector3 direction = Vector3.Normalize(waypoints[targetIndex] - ship.position);

            //add tilting and smooth turning here (if necessary and do-able)

            ship.up = ship.trackUp;
            ship.direction = direction;
            ship.right = Vector3.Cross(ship.direction, ship.up);

            //speed
            if (ship.speed < speeds[targetIndex])
            {
                //making the hard AI use the boost if posiible (limiting boost to the later straights, not the beggining)
                if (GameManager.AIdifficulty == 2 && (piece1.trackType == 1 || piece1.trackType == 2) && (piece2.trackType == 1 || piece2.trackType == 2) && (piece3.trackType == 1 || piece3.trackType == 2) && ship.trackNumber > 40 && ship.boost >= 50)
                {
                    ship.useBoost();
                    boosting = true;
                }
                ship.accelerate();
            }
            else
            {
                if (!boosting)
                    ship.brake();
            }
            if (ship.speed <= 0)
                ship.accelerate();



            //using power-ups
            if (ship.power1 != null)
            {
                if (GameManager.Random100 == 0)
                    ship.usePower1();
            }
            else if (ship.power2 != null)
            {
                if (GameManager.Random100 == 0)
                    ship.usePower2();
            }

            //force = direction * acceleration
            ship.speed += ship.acceleration * ship.time;

            //drag
            ship.speed *= 1 - (0.005f * ship.dragFactor);

            ship.position += ship.speed * ship.direction * ship.time;
        }
    }
}
