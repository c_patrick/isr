﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using System.IO;

//collision behaviour
//interacting with other ships
//turn functionality into methods for AI use

namespace ISR
{
    //the generalized ship class
    class Ship
    {
        #region Variables
        //graphics device
        private GraphicsDevice graphicsDevice;
        //elapsed time
        public float time = 0.0f;
        public Boolean countingDown = true;
        #endregion

        #region Properties
        //ship properties
        public Model model;

        //cameras
        public List<Camera> cameras; //add cameras to this for easier access to all
        public ChaseCamera chaseCamera;
        public FPSCamera fpsCamera;
        public ReverseCamera reverseCamera;
        public int currentCamera = 0;
        public int previousCamera = 0;
        int buttonDelay = 0;

        #region Physical
        //physical properties
        public float maxSpeed = 250.0f;
        public float dragFactor = 0.66f;
        public float rotationFactor = 1.2f;
        public float defaultForce = 40.0f;
        public float accelerationForce = 40.0f;
        public int gear;

        //boost
        public float boost = 0.0f;
        float boostTime = 0.0f;
        float timeToBoost = 3.0f;
        bool boosting;
        #endregion

        #region Position and Speed
        //properties specific to speed and position
        public float altitude = 5.0f;
        public Vector3 position;
        public Vector3 direction;
        public Vector3 up;
        public Vector3 right;
        Vector3 force;
        public Matrix world;
        public float speed;
        public Vector3 trackUp;
        public float tilt;
        public float totalRotation;
        public int trackNumber = 0;
        public TrackPiece currentPiece = null;
        public TrackPiece nextPiece = null;

        #endregion

        #region Player Details
        //keeping track of player and ladder positions
        public AIController agent;
        public PlayerController playerC;
        public int player;
        public int startPosition;
        public float ranking;
        public int piecesCovered;
        public float distance;
        public int lap;
        public int laps;
        public int lapLength;
        public bool started;
        public bool finished;
        public float lapTime;
        public float bestLap;
        public float totalTime;
        public int currentCheckpoint;
        public float splitTime;

        //powerups
        public Powerup power1 = null;
        public Powerup power2 = null;

        //actived powerups
        public MineDrop mine = null;
        public RocketDrop rocket = null;

        //variables/objects used in methods
        KeyboardState keys;
        GamePadState pad;
        public float acceleration;
        public float rotationChange;

        //for waypoint collection
        //public FileStream fs;
        //public StreamWriter sw;
        //float timePassed = 0;

        #endregion

        #endregion

        #region Constructor
        //constructor
        public Ship(GraphicsDevice device, Model shipModel, int playerNumber, int positionNumber)
        {
            //fs = new FileStream("Content/points.txt", FileMode.Create);
            //sw = new StreamWriter(fs);

            if (playerNumber > GameManager.hPlayers)
            {
                agent = new AIController();
            }
            else
            {
                playerC = new PlayerController();
                agent = null;
            }
            graphicsDevice = device;
            model = shipModel;
            cameras = new List<Camera>();
            chaseCamera = new ChaseCamera();
            fpsCamera = new FPSCamera();
            reverseCamera = new ReverseCamera();
            startPosition = positionNumber;
            player = playerNumber;
            cameras.Add(chaseCamera);
            cameras.Add(fpsCamera);
            cameras.Add(reverseCamera);
            lapLength = 0;
            Reset();
        }

        #endregion

        #region Respawn and Reset
        //the reset function
        public void Reset()
        {
            speed = 0;
            force = Vector3.Zero;
            position = new Vector3((-5 + (1 - (startPosition % 2)) * 10), 0, 0 + ((int)Math.Floor((double)(startPosition - 1) / 2)) * 8);
            direction = Vector3.Forward;
            up = Vector3.Up;
            right = Vector3.Right;
            world = Matrix.Identity;
            world.Forward = Vector3.Forward;
            world.Up = Vector3.Up;
            world.Right = Vector3.Right;
            world.Translation = position;
            ranking = startPosition;
            gear = 1;
            tilt = 0.0f;
            totalRotation = 0;
            boost = 100;
            boosting = false;
            lap = 0;
            lapTime = 0;
            bestLap = 0;
            totalTime = 0;
            started = false;
            finished = false;
            piecesCovered = 0;
            trackNumber = 0;
            power1 = null;
            power2 = null;
            mine = null;
            rocket = null;
            countingDown = true;
            if (agent != null)
                agent.targetIndex = 0;
            currentCheckpoint = 0;
        }

        //respawining the ship at the desired location
        public void Respawn(Vector3 spawnPoint, Vector3 lookAt, float trackYaw, int trackType, Matrix transformation)
        {
            Vector3 trackDirection = Vector3.Normalize(lookAt - spawnPoint);
            float lookAtAngle = (float)(Math.Acos(Vector3.Dot(trackDirection, Vector3.Forward)));
            if (lookAt.X > (spawnPoint.X + 20))
                lookAtAngle = -lookAtAngle;

            speed = 0;
            force = Vector3.Zero;
            position = Vector3.Transform(new Vector3((-5 + (1 - (startPosition % 2)) * 10), 0, 0 + ((int)Math.Floor((double)(startPosition - 1) / 2)) * 8), transformation); // spawnPoint;
            direction = Vector3.Forward;
            up = Vector3.Up;
            right = Vector3.Right;
            world = Matrix.Identity;
            world.Forward = Vector3.Forward;
            world.Up = Vector3.Up;
            world.Right = Vector3.Right;
            world.Translation = position;
            gear = 1;
            tilt = 0.0f;
            totalRotation = lookAtAngle;
            boosting = false;
        }
        #endregion

        #region Update
        //the update method
        public void Update(GameTime gameTime, GamePadState[] pads)
        {
            //getting the elapsed time between updates
            time = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //updating lap status - won't work for reverse tracks - will need a separate check for that
            if (started && !finished)
            {
                lapTime += time;
                if (trackNumber < piecesCovered + 3 && trackNumber > piecesCovered - 1)
                    piecesCovered = trackNumber;
                if (trackNumber == 1 && piecesCovered >= lapLength - 2 && position.Z < 0)
                {
                    lap++;
                    piecesCovered = trackNumber;
                    if (bestLap == 0 || lapTime < bestLap)
                        bestLap = lapTime;
                    totalTime += lapTime;
                    lapTime = 0;
                }
                if (lap > laps)
                    finished = true;
            }
            else if (!countingDown && !started)
            {
                if (position.Z <= 0)
                {
                    started = true;
                    lap = 1;
                }
            }
            else if (finished)
            {
                position += speed * direction * time;
                unTilt();
            }

            //updating boost
            if (boosting == true)
            {
                boost -= (100 / 3 * time);
                accelerationForce = defaultForce + 20;
                boostTime -= time;
            }
            else
            {
                accelerationForce = defaultForce;
                //accumulating boost
                boost += time;
                if (boost > 100)
                    boost = 100;
            }

            //rotations
            rotationChange = 0.0f;

            //speed
            acceleration = 0.0f;

            //controls cannot be used while countdown is still active
            if (!countingDown && !finished)
            {
                //getting button states if the controller is a player
                if (agent == null)
                    playerC.Update(this);
                else
                    agent.Update(this);
            }
            //world matrix
            world = Matrix.Identity;
            world.Up = up;
            world.Forward = direction;
            world.Right = Vector3.Cross(direction, up);
            world.Translation = position;

            //updating the cameras
            UpdateCameras(gameTime);

            //writing waypoints
            //timePassed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            //if (timePassed >= 100 && lap == 2)
            //{
            //    sw.WriteLine(position.X + " " + position.Y + " " + position.Z + " " + speed);
            //    timePassed -= 100;
            //}
        }

        private void UpdateCameras(GameTime gameTime)
        {
            //updating the chase camera
            chaseCamera.targetPosition = position;
            chaseCamera.targetDirection = direction;
            chaseCamera.Update(gameTime);
            //updating the fps camera
            fpsCamera.targetPosition = position;
            fpsCamera.targetDirection = direction;
            fpsCamera.Update(gameTime);
            //updating the reverse camera
            reverseCamera.targetPosition = position;
            reverseCamera.targetDirection = direction;
            reverseCamera.Update(gameTime);
        }
        #endregion

        //methods for control so that AI can use them
        //using the first power-up
        public void usePower1()
        {
            if (power1 != null)
            {
                if (power1.type == 6)
                    mine = power1.activateMine(position + -5 * direction);
                else
                    rocket = power1.activateRocket(position + 7 * direction, direction);
                power1 = null;
            }
        }

        //using the second power-up
        public void usePower2()
        {
            if (power2 != null)
            {
                if (power2.type == 6)
                    mine = power2.activateMine(position + -5 * direction);
                else
                    rocket = power2.activateRocket(position + 7 * direction, direction);
                power2 = null;
            }
        }

        //using boost
        public void useBoost()
        {
            if (boosting == false)
            {
                boosting = true;
                boostTime = timeToBoost * (boost / 100.0f);
            }
            if (boost <= 0)
                boosting = false;
        }

        //stopping boost
        public void cutBoost()
        {
            boosting = false;
        }

        //turning left
        public void turnLeft()
        {
            tilt -= 0.02f;
            float stickFactor = 1;
            if (pad.ThumbSticks.Left.X < 0)
                stickFactor = Math.Abs(pad.ThumbSticks.Left.X);
            if (speed >= 0)
                rotationChange += rotationFactor * time * stickFactor;
            else
                rotationChange -= rotationFactor * time * stickFactor;
        }

        //turning right
        public void turnRight()
        {
            tilt += 0.02f;
            float stickFactor = 1;
            if (pad.ThumbSticks.Left.X > 0)
                stickFactor = pad.ThumbSticks.Left.X;
            if (speed >= 0)
                rotationChange -= rotationFactor * time * stickFactor;
            else
                rotationChange += rotationFactor * time * stickFactor;
        }

        //regaining normal tilt
        public void unTilt()
        {
            if (tilt < -0.02f)
                tilt += 0.02f;
            else if (tilt > 0.02f)
                tilt -= 0.02f;
            else
                tilt = 0.0f;
        }

        //moving forwards
        public void accelerate()
        {
            float triggerFactor = 1;
            if (pad.Triggers.Right > 0)
                triggerFactor = pad.Triggers.Right;
            acceleration = accelerationForce * triggerFactor;
        }

        //moving backwards/slowing down
        public void brake()
        {
            float triggerFactor = 1;
            if (pad.Triggers.Left > 0)
                triggerFactor = pad.Triggers.Left;
            acceleration = -accelerationForce * triggerFactor;
        }
    }
}
