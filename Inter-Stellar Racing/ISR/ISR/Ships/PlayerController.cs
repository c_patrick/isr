﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ISR
{
    class PlayerController
    {
        KeyboardState keys;
        GamePadState pad;
        Ship ship;

        int buttonDelay = 0;

        public void Update(Ship _ship)
        {
            ship = _ship;

            ship.acceleration = 0;

            //getting the current input state
            keys = InputState.keyboard;
            pad = InputState.gamePads[ship.player - 1];

            //respawning the ship
            if (pad.IsButtonDown(Buttons.Back))
                ship.Respawn(ship.currentPiece.position, ship.nextPiece.position, ship.currentPiece.yaw, ship.currentPiece.trackType, ship.currentPiece.world);

            //using the powerups
            if (keys.IsKeyDown(Keys.O) || pad.IsButtonDown(Buttons.Y))
                ship.usePower1();
            if (keys.IsKeyDown(Keys.P) || pad.IsButtonDown(Buttons.X))
                ship.usePower2();

            //changing the camera
            if ((keys.IsKeyDown(Keys.X) || pad.IsButtonDown(Buttons.LeftShoulder)) && buttonDelay <= 0)
                ship.currentCamera = 2;
            else
                ship.currentCamera = ship.previousCamera;

            if (((keys.IsKeyDown(Keys.C) && keys.IsKeyUp(Keys.X)) || (pad.IsButtonDown(Buttons.RightShoulder)) && buttonDelay <= 0 && pad.IsButtonUp(Buttons.LeftShoulder)))
            {
                if (ship.currentCamera == 1)
                    ship.currentCamera = 0;
                else if (ship.currentCamera == 0)
                    ship.currentCamera = 1;
                ship.previousCamera = ship.currentCamera;
                buttonDelay = 15;
            }
            buttonDelay--;

            //using the boost
            if (keys.IsKeyDown(Keys.LeftShift) || pad.IsButtonDown(Buttons.A))
                ship.useBoost();
            else
                ship.cutBoost();

            //adjusting tilting and rotation angles
            if (pad.IsButtonDown(Buttons.DPadLeft) || pad.ThumbSticks.Left.X < 0 || keys.IsKeyDown(Keys.A))
                ship.turnLeft();
            else if (pad.IsButtonDown(Buttons.DPadRight) || pad.ThumbSticks.Left.X > 0 || keys.IsKeyDown(Keys.D))
                ship.turnRight();
            else if (((pad.IsButtonUp(Buttons.DPadLeft) && pad.IsButtonUp(Buttons.DPadRight)) && pad.ThumbSticks.Left.X == 0) || (keys.IsKeyUp(Keys.D) && keys.IsKeyUp(Keys.A)))
                ship.unTilt();

            if (keys.IsKeyDown(Keys.W) || pad.IsButtonDown(Buttons.DPadUp) || pad.Triggers.Right > 0)
                ship.accelerate();
            if (keys.IsKeyDown(Keys.S) || pad.IsButtonDown(Buttons.DPadDown) || pad.Triggers.Left > 0)
                ship.brake();

            //restricting the tilt angle
            if (ship.tilt > 0.3f)
                ship.tilt = 0.3f;
            else if (ship.tilt < -0.3f)
                ship.tilt = -0.3f;

            //accumulative rotation
            ship.totalRotation += ship.rotationChange;
            //implementing rotation + tilt
            ship.up = ship.trackUp;
            ship.direction = Vector3.TransformNormal(Vector3.Forward, Matrix.CreateFromAxisAngle(ship.up, ship.totalRotation));
            ship.up = Vector3.TransformNormal(ship.up, Matrix.CreateFromAxisAngle(ship.direction, ship.tilt));
            ship.right = Vector3.Cross(ship.direction, ship.up);

            //force = direction * acceleration
            ship.speed += ship.acceleration * ship.time;

            //drag
            ship.speed *= 1 - (0.005f * ship.dragFactor);

            //limiting speed
            if (ship.speed > ship.maxSpeed)
                ship.speed = ship.maxSpeed;
            else if (ship.speed < -ship.maxSpeed / 2)
                ship.speed = -ship.maxSpeed / 2;

            if (ship.acceleration == 0)
            {
                if (ship.speed > 0 && ship.speed < 0.1f)
                    ship.speed = 0;
                else if (ship.speed < 0 && ship.speed > -0.1f)
                    ship.speed = 0;
            }

            //updating position
            ship.position += ship.speed * ship.direction * ship.time;

            //updating gear value
            ship.gear = 1 + (int)(5 * ship.speed / ship.maxSpeed);
        }
    }
}
