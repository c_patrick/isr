using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public static int SCREEN_WIDTH = 1366;
        public static int SCREEN_HEIGHT = 768;

        //standard variables
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ScreenManager screenManager;
        TrackManager trackManager, track1,track2;
        

        //constructor
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
          //  graphics.ToggleFullScreen();
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = SCREEN_WIDTH;
            graphics.PreferredBackBufferHeight = SCREEN_HEIGHT;
            ContentLoader.Initialise(this);// sets the content loader 
            GameManager.Initialise();// randomisies race settings to make quick races different
            
            IsMouseVisible = true;
            screenManager = new ScreenManager(this,graphics);

            // create the track managers
            track1 = new TrackManager(this,0);
            track2 = new TrackManager(this,1);

            track1.Initialize();
            track2.Initialize();


            // pass the tracks to the screen manager where they can be assigned to a gameplay class
            screenManager.addTrack(track1);
            screenManager.addTrack(track2);

            // add all the screens to the screen manager
            screenManager.addScreen(new MainMenuScreen(), false, false);
            screenManager.addScreen(new PauseScreen(), false, false);
            screenManager.addScreen(new ResultsScreen(), false, false);
            screenManager.addScreen(new ControlScreen(), false, false);
            screenManager.addScreen(new TrackSelectionScreen(), false, false);
            screenManager.addScreen(new ShipSelectionSreen(), false, false);
            screenManager.addScreen(new ShipSelectionSreen(), false, false);
            screenManager.addScreen(new SettingsScreen(), false, false);
            screenManager.addScreen(new StartScreen(), true, true);
            
        }

        //initialising
        protected override void Initialize()
        {
            base.Initialize();
            screenManager.Initialize();
           // trackManager.Initialize();

        }

        //loading content
        protected override void LoadContent()
        {
            Viewport vp = graphics.GraphicsDevice.Viewport;
            screenManager.LoadContent(graphics.GraphicsDevice);
            //ContentLoader.LoadContent(content);
           // trackManager.Initialize();
        }

        protected override void UnloadContent()
        { }

        //the update method
        protected override void Update(GameTime gameTime)
        {
            screenManager.Update(gameTime);
            base.Update(gameTime);
        }


        //drawing the game assets
        protected override void Draw(GameTime gameTime)
        {
            screenManager.Draw(gameTime);
        }

        //drawing the HUD, in this case testing text
        private void DrawHUD(int selectShip)
        {

        }
    }
}
