﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ISR
{
    public class Powerup
    {
        #region Properties
        //properties
        public Vector3 position;
        public Model pickModel;
        public Model dropModel;
        public float pitch;
        public float yaw;
        public int type;

        //matrices
        public Matrix world;
        public Matrix rotationMatrix;

        //acquiring
        public bool acquired = false;
        int countdown = 15;
        double timeElapsed = 0;

        #endregion

        #region Constructor
        //constructor
        public Powerup(Vector3 location, Model pModel, Model dModel, int pType)
        {
            //assigning properties
            position = location;
            pickModel = pModel;
            dropModel = dModel;
            pitch = 0;
            yaw = 0;
            type = pType;

            //initialising the matrix
            world = Matrix.Identity;
            world.Up = Vector3.Up;
            world.Forward = Vector3.Forward;
            world.Right = Vector3.Right;
            rotationMatrix = Matrix.CreateRotationX(pitch) * Matrix.CreateRotationY(yaw);
            world *= rotationMatrix;
            world.Translation = position;
        }
        #endregion

        #region Update
        //updating the animations
        public void Update(double elapsedTime)
        {
            yaw += (float)elapsedTime * 0.002f;

            timeElapsed += elapsedTime;

            if (timeElapsed >= 1000)
            {
                timeElapsed = 0;
                if (acquired)
                    countdown--;
            }
            if (countdown <= 0)
            {
                acquired = false;
                countdown = 30;
            }

            //initialising the matrix
            world = Matrix.Identity;
            world.Up = Vector3.Up;
            world.Forward = Vector3.Forward;
            world.Right = Vector3.Right;
            rotationMatrix = Matrix.CreateRotationX(pitch) * Matrix.CreateRotationY(yaw);
            world *= rotationMatrix;
            world.Translation = position;

        }
        #endregion

        #region Usage Methods
        //acquiring a power up
        public Powerup acquire()
        {
            acquired = true;

            return this;
        }

        //using the power up
        public MineDrop activateMine(Vector3 location)
        {
            MineDrop mine = new MineDrop(location, dropModel);
            return mine;
        }

        public RocketDrop activateRocket(Vector3 location, Vector3 direction)
        {
            RocketDrop rocket = new RocketDrop(location, dropModel, direction);
            return rocket;

        }
        #endregion
    }
}
