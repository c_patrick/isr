﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ISR
{
    public class RocketDrop
    {
        #region Properties
        //properties
        public Vector3 position;
        public Vector3 direction;
        public Model model;
        public int speed = 350;

        //matrices
        public Matrix world;

        public double lifetime = 5000;
        #endregion

        #region Constructor
        //constructor
        public RocketDrop(Vector3 location, Model powerModel, Vector3 dir)
        {
            //assigning properties
            position = location;
            direction = dir;
            model = powerModel;

            //initialising the matrix
            world = Matrix.Identity;
            world.Up = Vector3.Up;
            world.Forward = direction;
            world.Right = Vector3.Cross(direction, Vector3.Up);
            world.Translation = position;
        }
        #endregion

        #region Update
        public void Update(double time)
        {
            position += (float)(time / 1000) * speed * direction;
            world.Translation = position;
        }
        #endregion

        #region Drawing
        public void DrawP(Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                    //lighting
                    effect.DirectionalLight0.DiffuseColor = new Vector3(0.3f, 0.3f, 0.3f);
                    effect.DirectionalLight0.Direction = new Vector3(5, 10, 4);
                    effect.DirectionalLight0.Enabled = true;
                    effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                    effect.DirectionalLight1.DiffuseColor = new Vector3(0.2f, 0.2f, 0.2f);
                    effect.DirectionalLight1.Direction = new Vector3(-5, -10, -4);
                    effect.DirectionalLight1.Enabled = true;
                    effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                    effect.DirectionalLight2.DiffuseColor = new Vector3(0, 0.2f, 0.7f);
                    effect.DirectionalLight2.Direction = new Vector3(-2, -10, -2);
                    effect.DirectionalLight2.Enabled = true;
                    effect.DirectionalLight2.SpecularColor = new Vector3(0.3f, 0.1f, 0.1f);

                    effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.LightingEnabled = true;
                }
                mesh.Draw();
            }
        }
        #endregion
    }
}
