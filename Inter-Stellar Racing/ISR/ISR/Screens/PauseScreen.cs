﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class PauseScreen: GameScreen
    {
        // needs a new background - possibly some sort of blurred version of the track or something
        #region variables

        private int menuDelay = 0;
            private int menuSelect = 0;
            private Texture2D background1, background2, pillars, selectionBar;
            private SpriteBatch spriteBatch;
            private GraphicsDeviceManager graphics;

        #endregion

        public PauseScreen()
        {
            graphics = new GraphicsDeviceManager(this);
        }

        public override void Initialize()
        {
           // throw new NotImplementedException();
        }


        protected override void LoadContent()
        {
            throw new NotImplementedException();
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            spriteBatch = this.screenMan.SpriteBatch;

            selectionBar = content.Load<Texture2D>("pauseSelectionBar");
            background1 = ContentLoader.Menu_Elements[15];
            background2 = content.Load<Texture2D>("backgroundPillars");

        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            menuDelay++;

            //throw new NotImplementedException();
            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                // should have transitioning (i.e. fading to black) happening here
                screenMan.changeScreen(this,9,false);
                   // this.screenMan.changeScreen(this, 1, false);
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Enter) || InputState.gamePads[0].IsButtonDown(Buttons.A)))
            {
                 // this.screenMan.changeScreen(this, 0, false);
                switch (menuSelect)
                {
                    default: screenMan.changeScreen(this, 9, false); break;
                    case (1): screenMan.RestartRace(); screenMan.changeScreen(this, 9, false); break;
                    case (2): screenMan.endGame(this, 0); break;
                }
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Down) || InputState.gamePads[0].IsButtonDown(Buttons.DPadDown)) && menuDelay > 10)
            {
                menuDelay = 0;
                menuSelect ++;
                if (menuSelect > 2)
                    menuSelect = 0;
                
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Up) || InputState.gamePads[0].IsButtonDown(Buttons.DPadUp)) && menuDelay > 10)
            {
                menuDelay = 0;
                menuSelect--;
                if (menuSelect < 0)
                    menuSelect = 2;
            }
        }

        public override void  Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            String[] menuOptions = {"Resume Race","Restart Race","Quit Race"};

            Color alpha = new Color(255, 255, 255, transFade);

            spriteBatch.Draw(background1,new Rectangle(0,0,screenWidth,screenHeight),alpha);
            
            spriteBatch.Draw(selectionBar, new Vector2(screenWidth / 2 -selectionBar.Width/2-50, 
                screenHeight / 4 + (menuSelect * 150)), alpha);
          //  spriteBatch.DrawString(ContentLoader.HUD_Font[2], menuOptions[menuSelect], 
          //     new Vector2(screenWidth / 2 - 170, screenHeight / 4 + (menuSelect * 150) +25) ,alpha);

            
            spriteBatch.Draw(ContentLoader.Menu_Elements[8], new Vector2(screenWidth / 2 + ContentLoader.Menu_Elements[8].Width +100,
                screenHeight - ContentLoader.Menu_Elements[8].Height - 50), alpha);

            spriteBatch.Draw(ContentLoader.Menu_Elements[9 + menuSelect], new Vector2(screenWidth / 2 - ContentLoader.Menu_Elements[9 + menuSelect].Width / 2 - 25,
                screenHeight / 4 + (menuSelect * 150) + ContentLoader.Menu_Elements[9 + menuSelect].Height / 2), alpha);

            spriteBatch.Draw(background2, new Rectangle(0, ContentLoader.Menu_Elements[2].Height -38, background2.Width, screenHeight), alpha);
            spriteBatch.Draw(ContentLoader.HUD_Elements[0], new Rectangle(0, 0, screenWidth, ContentLoader.HUD_Elements[0].Height), alpha);
        }



    }
}
