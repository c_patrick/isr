﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    /// <summary>
    /// Class to store the results of the game
    /// </summary>
    public class ResultsScreen:GameScreen
    {
        Texture2D titleBar;
        Texture2D leftPlace, rightPlace;

        public ResultsScreen()
        {
 
        }
        public override void Initialize()
        {
            spriteBatch = screenMan.SpriteBatch;
            titleBar = ContentLoader.Menu_Elements[4];
            leftPlace = ContentLoader.Menu_Elements[21];
            rightPlace = ContentLoader.Menu_Elements[22];
         //   throw new NotImplementedException();
        }

        public override void LoadContent(ContentManager content)
        {
       //     throw new NotImplementedException();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {

            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                GameManager.Positioning.Clear();

                nextScreen = 0;
                exiting = true;
            }
           // throw new NotImplementedException();
        }
        int height = 250;
        public override void Draw(GameTime gameTIme)
        {
            spriteBatch.Draw(titleBar, new Rectangle(0, 75,screenWidth,titleBar.Height), alpha);
            

            spriteBatch.Draw(leftPlace, new Vector2(0, height), alpha);
            spriteBatch.Draw(leftPlace, new Vector2(0, height+100), alpha);
            spriteBatch.Draw(leftPlace, new Vector2(0, height+200), alpha);
            spriteBatch.Draw(leftPlace, new Vector2(0, height+300), alpha);

            spriteBatch.Draw(rightPlace, new Vector2(screenWidth - rightPlace.Width, height+50), alpha);
            spriteBatch.Draw(rightPlace, new Vector2(screenWidth - rightPlace.Width, height+150), alpha);
            spriteBatch.Draw(rightPlace, new Vector2(screenWidth - rightPlace.Width, height+250), alpha);
            spriteBatch.Draw(rightPlace, new Vector2(screenWidth - rightPlace.Width, height+350), alpha);

            for (int x = 0; x < 4 && x < GameManager.Positioning.ToArray().Length; x++)
            {
                spriteBatch.DrawString(ContentLoader.HUD_Font[0], "Pos: "+(x+1)+" - Ship: " + GameManager.Positioning[x].ranking + "  - Time: " + GameManager.Positioning[x].totalTime, new Vector2(50, x*100 + height + 25), Color.White);
            }
            for (int x = 4; x < 8 && x < GameManager.Positioning.ToArray().Length; x++)
            {
                spriteBatch.DrawString(ContentLoader.HUD_Font[0], "Pos: " + (x + 1) + " - Ship: " + GameManager.Positioning[x].ranking + "  - Time: " + GameManager.Positioning[x].totalTime, new Vector2(screenWidth / 2 + 250, (x - 4) * 100 + 50 + height + 25), Color.White);
            }
            //GameManager.Positioning.Clear();
           // throw new NotImplementedException();
        }
    }
}
