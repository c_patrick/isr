﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace ISR
{
    class TrackSelectionScreen: GameScreen
    {

        int selectedTrack = 0;
        Texture2D background,menuTitle;
        Texture2D track1, track2, controls3;
        Texture2D[] tracks = new Texture2D[2];
        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;


        public TrackSelectionScreen()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
        }

        private void changeScreens()
        {
            if ((InputState.keyboard.IsKeyDown(Keys.Left) || InputState.gamePads[0].IsButtonDown(Buttons.DPadLeft)))
            {
               
                selectedTrack --;
                if (selectedTrack < 0)
                    selectedTrack = tracks.Length - 1 ;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Right) || InputState.gamePads[0].IsButtonDown(Buttons.DPadRight)))
            {

                selectedTrack++;
                if (selectedTrack >= tracks.Length)
                    selectedTrack = 0;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Space) || InputState.gamePads[0].IsButtonDown(Buttons.A)))
            {
                // should have transitioning (i.e. fading to black) happening here
                //if (nextScreen == -1)
                GameManager.TrackSelect = selectedTrack;
                nextScreen = 7;
                exiting = true;
                //this.screenMan.changeScreen(this, 0, false);
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                // should have transitioning (i.e. fading to black) happening here
                    nextScreen = 0;
                exiting = true;
                //this.screenMan.changeScreen(this, 0, false);
            }
        }

        # region Load and Initialize

        public override void LoadContent(ContentManager content)
        {
         //   tracks = new Texture2D[2];
            spriteBatch = this.screenMan.SpriteBatch;

            menuTitle = ContentLoader.Menu_Elements[6];//content.Load<Texture2D>("MenuItems/menu_bar - heading");
            tracks[0] = content.Load<Texture2D>("MenuItems/track1");
            tracks[1] = content.Load<Texture2D>("MenuItems/track2");
            controls3 = content.Load<Texture2D>("MenuItems/item_controls2");
        }
        public override void Initialize()
        {
            
        }

        # endregion

        #region Update and Draw

        public override void Update(GameTime gameTime)
        {
            menuDelay++;
            //a delay for the buttons
            if (exiting)
            {
                transFade -= 0.05f;
                if (transFade <= 0)
                {
                    exiting = false;
                    transFade = 1.0f;
                    this.screenMan.changeScreen(this, nextScreen, false);
                }
            }
                
            if (menuDelay >= 10 && !exiting)
            {
                changeScreens();
                menuDelay = 0;
                //changeTrack(); 
            }

                
        }
        public override void Draw(GameTime gameTime)
        {
            //spriteBatch.Begin();
            int centre_x = 1366 / 2;
            int centre_y = 768 / 2;

            Color alpha = new Color(255,255,255,transFade);

            // draw the title
            //spriteBatch.Draw(background, new Vector2(0, 0),
            //   new Rectangle(0, 0, 1280, 720), Color.White);
            spriteBatch.Draw(menuTitle, new Rectangle(0, 75, centre_x * 2, menuTitle.Height), Color.White);

            // draw the menu options
            spriteBatch.Draw(ContentLoader.Menu_Elements[13+selectedTrack],
                new Vector2(centre_x - tracks[selectedTrack].Width / 2, centre_y - tracks[selectedTrack].Height / 3), alpha);

         //   spriteBatch.Draw(controls1,
           //     new Vector2(centre_x + controls1.Width, centre_y + controls1.Height * 2), Color.White);
            spriteBatch.Draw(controls3,
                new Vector2(centre_x + (int)(controls3.Width), centre_y * 2 -controls3.Height- 50), alpha);
            //spriteBatch.End();
        }

        #endregion
    }
}
