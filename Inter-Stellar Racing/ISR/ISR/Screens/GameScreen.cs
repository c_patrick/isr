﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public abstract class GameScreen : Microsoft.Xna.Framework.Game
    {
        public ScreenManager screenMan;
        public TrackManager trackMan;
        protected SpriteBatch spriteBatch;
        protected float pulseFade = 1.0f,
            transFade = 1.0f, menuDelay = 0f;
        
        bool drawn = false;
        bool updated = false;

        protected int nextScreen = -1;
        protected int screenWidth = 1366;
        protected int screenHeight = 768;
        protected bool exiting = false;
        protected bool entering = false;
        protected bool visibility = false;
        protected Color alpha = Color.White;

        

        public TrackManager trackManager
        {
            set { trackMan = value; }
            get { return trackMan; }
        }
        
        public ScreenManager screenManager
        {
            set { screenMan = value; }
            get { return screenMan; }
        }

        public bool Drawn
        {
            set { drawn = value; }
            get { return drawn; }
        }

        public bool Updated
        {
            set { updated = value; }
            get { return updated; }
        }
        public bool EnterScreen
        {
            set { entering = value; transFade = 0f; }
            get { return entering; }
        }
        public abstract void Initialize();
        public abstract void LoadContent(ContentManager content);
        
        /// <summary>
        /// Logic step which calls update unless the screen is entering or exiting, then it reduces the fade value to account 
        /// the screen elements fading out
        /// </summary>
        /// <param name="gt"></param>
        
        public virtual void Logic(GameTime gt)
        {
            // exits and entrances
            if (exiting)
            {
                transFade -= 0.05f;
                if (transFade <= 0)
                {
                    transFade = 1.0f;
                    exiting = false;
                    // transition to set screen
                    screenMan.changeScreen(this,nextScreen,visibility);
                }
            }
            else if (entering)
            {
                transFade += 0.05f;
                if (transFade >= 1)
                {
                    transFade = 1.0f;
                    entering = false;
                }
            }

             Update(gt);
            
        }
        
        public abstract void Update(GameTime gameTime);
        
        public abstract void Draw(GameTime gameTime);

        public virtual void RestartRace()
        {
            // not implemented for most classes 
        }

    }
}
