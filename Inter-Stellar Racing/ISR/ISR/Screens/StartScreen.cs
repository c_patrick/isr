﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class StartScreen: GameScreen
    {
        bool pulsingUp = false;

        SpriteBatch spriteBatch;
        Texture2D button;
        Texture2D background;

        #region Initialize and Load Content
        public override void Initialize()
        {
            pulseFade = 0.20f;
            transFade = 0.90f;

            spriteBatch = screenMan.SpriteBatch;
            button = ContentLoader.Menu_Elements[1];
            background = ContentLoader.Menu_Elements[0];
            //throw new NotImplementedException();
            MediaPlayer.Volume = 0.5f;
            MediaPlayer.Play(ContentLoader.MusicElements[0]);
        }

        public override void LoadContent(ContentManager content)
        {
           // base.LoadContent();
        }
        #endregion

        #region Update and Draw
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
           // throw new NotImplementedException();

            if (pulsingUp)
                pulseFade += 0.010f;
            else
                pulseFade -= 0.01f;

            if (pulseFade >= 1.0f)
                pulsingUp = false;
            else if (pulseFade <= 0.09f)
                pulsingUp = true;
                

            // Return to the main menu if enter is pressed
            if ((InputState.keyboard.IsKeyDown(Keys.Space) || InputState.gamePads[0].IsButtonDown(Buttons.Start)))
            {
                screenMan.changeScreen(this, 0, false);
                MediaPlayer.Volume = 0.5f;

                
            }
                
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            Color alphaPulse = new Color((int)(255*pulseFade),255*pulseFade,255*pulseFade,pulseFade);
            Color alphaFade = new Color(255,255,255,transFade);
            int pulseSize = (int)(15 * pulseFade);

            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), alphaFade);
            spriteBatch.Draw(button, new Rectangle(screenWidth / 2 - button.Width / 2 - pulseSize/2,
                screenHeight / 2 + button.Height + 200 - pulseSize / 2, button.Width + (int)pulseSize, button.Height + (int)pulseSize), alphaPulse);
            //throw new NotImplementedException();
        }
        #endregion
    }
}
