﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class ShipSelectionSreen:GameScreen
    {

        float modelTime;
        Ship[] ship = new Ship[4];
        int[] shipModel = {0,0,0,0};
        bool[] active = new bool[4];


        private void changeScreens()
        {
            for (int i = 0; i < 4; i++)
            {
                if (active[i])
                {
                    if (InputState.gamePads[i].IsButtonDown(Buttons.DPadLeft))
                    {
                        shipModel[i] += 1;
                        if (shipModel[i] >= 9)
                            shipModel[i] = 0;

                        ship[i].model = ContentLoader.Ship_Models[shipModel[i]];
                    }
                    if (InputState.gamePads[i].IsButtonDown(Buttons.DPadRight))
                    {
                        shipModel[i] -= 1;
                        if (shipModel[i] <= 0)
                            shipModel[i] = 8;

                        ship[i].model = ContentLoader.Ship_Models[shipModel[i]];
                    }
                    if (InputState.gamePads[i].IsButtonDown(Buttons.DPadDown))
                    {
                        shipModel[i] += 3;
                        if (shipModel[i] >= 9)
                            shipModel[i] -= 9;

                        ship[i].model = ContentLoader.Ship_Models[shipModel[i]];
                    }
                    if (InputState.gamePads[i].IsButtonDown(Buttons.DPadUp))
                    {
                        shipModel[i] -= 3;
                        if (shipModel[i] < 0)
                            shipModel[i] += 9;

                        ship[i].model = ContentLoader.Ship_Models[shipModel[i]];
                    }
                    if ((InputState.keyboard.IsKeyDown(Keys.Space) || InputState.gamePads[0].IsButtonDown(Buttons.A)))
                    {
                        int humans = 0;

                        for (int x = 0; x < 4; x++)
                        {
                            GameManager.humanModels[x] = shipModel[x];

                            if (active[x])
                                ++humans;
                        
                        }
                        GameManager.hPlayers = humans;
                        
                        nextScreen = 4;
                        exiting = true;
                        
                        //this.screenMan.changeScreen(this, 0, false);
                    } 
                }
                if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
                {
                    nextScreen = 0;
                    exiting = true;
                    
                    //this.screenMan.changeScreen(this, 0, false);
                }
                if (InputState.gamePads[i].IsButtonDown(Buttons.Start))
                {
                    active[i] = true;
                }
                if (InputState.keyboard.IsKeyDown(Keys.Enter))
                {
                    for (int j = 0; j < 4; j++ )
                        active[j] = true;
                }
            }
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent();
        }
        public override void Initialize()
        {
            spriteBatch = screenMan.SpriteBatch;
            ship[0] = new Ship(screenMan.GraphicsDevice, ContentLoader.Ship_Models[shipModel[0]], 0, 0);
            ship[1] = new Ship(screenMan.GraphicsDevice, ContentLoader.Ship_Models[shipModel[1]], 0, 0);
            ship[2] = new Ship(screenMan.GraphicsDevice, ContentLoader.Ship_Models[shipModel[2]], 0, 0);
            ship[3] = new Ship(screenMan.GraphicsDevice, ContentLoader.Ship_Models[shipModel[3]], 0, 0);
            
   
            //throw new NotImplementedException();
        }
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            modelTime = (float)gameTime.TotalGameTime.TotalSeconds;

            menuDelay++;
            //a delay for the buttons
            if (exiting)
            {
                
                transFade -= 0.05f;
                if (transFade <= 0)
                {
                    for (int j = 0; j < 4; j++)
                        active[j] = false;
                    exiting = false;
                    transFade = 1.0f;
                    this.screenMan.changeScreen(this, nextScreen, false);
                }
            }

            if (menuDelay >= 10 && !exiting)
            {
                changeScreens();
                menuDelay = 0;
                //changeTrack(); 
            }

            //throw new NotImplementedException();
        }
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {

            int[,] position = {{0,0,screenWidth/2,screenHeight/2},{screenWidth/2,0,screenWidth,screenHeight/2},
                              {0,screenHeight/2,screenWidth/2,screenHeight},{screenWidth/2,screenHeight/2,screenWidth,screenHeight}};

            for (int x = 0; x < 4; x++)
            {
                DrawModel(ship[x].model, ship[x].world, x);
                if (!active[x])
                {
                    spriteBatch.Draw(ContentLoader.Menu_Elements[15], new Rectangle(position[x, 0], position[x, 1],
                    position[x, 2], position[x, 3]), alpha);
                    // press 'start option'
                    spriteBatch.Draw(ContentLoader.Menu_Elements[16], new Rectangle(position[x,0]/2 + position[x,2]/2 - ContentLoader.Menu_Elements[16].Width/2
                        , position[x,1]/2+position[x,3]/2, ContentLoader.Menu_Elements[16].Width, ContentLoader.Menu_Elements[16].Height), alpha);
                }
                else
                {
                    spriteBatch.Draw(ContentLoader.Menu_Elements[17],
                        new Rectangle(position[x, 0] + 50, position[x, 3] - 50, 
                            ContentLoader.Menu_Elements[17].Width, ContentLoader.Menu_Elements[17].Height), alpha);

                    spriteBatch.Draw(ContentLoader.Menu_Elements[18],
                        new Rectangle(position[x, 2] - ContentLoader.Menu_Elements[18].Width-  50, position[x, 3]  - 50,
                            ContentLoader.Menu_Elements[18].Width, ContentLoader.Menu_Elements[18].Height), alpha);
                }
                
            }
            //title bar
            //spriteBatch.Draw(ContentLoader.Menu_Elements[2], new Rectangle(0,45,screenWidth,ContentLoader.Menu_Elements[2].Height), alpha);

            
            //throw new NotImplementedException();
        }
        private void DrawModel(Model model, Matrix world, int index)
        {
            //  Vector3 cameraPosition = new Vector3(0, 0, 0);

            // view and projection matrices
            int[,] locations = { { 0, 12 }, { -12, 12 }, { 0, 4 }, { -12, 4 } };

            // Animated the model rotating
            float modelRotation = modelTime/5f;
            if (!active[index])
                modelRotation = 45;
            // Set the positions of the camera in world space, for our view matrix.
            //camera = ship.cameras[0];

            Vector3 cameraPosition = new Vector3(-10.0f, 15.0f, -25.0f);
            Vector3 lookAt = new Vector3(-5.0f, 7.0f, 0.0f);

            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transforms[mesh.ParentBone.Index] *
                        Matrix.CreateRotationY(modelRotation) * Matrix.CreateTranslation(locations[index, 0], locations[index, 1], -6);
                    effect.View = Matrix.CreateLookAt(cameraPosition, lookAt,
                        Vector3.Up);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        MathHelper.ToRadians(45.0f), (float)(4.0f / 3.0f), 1.0f, 10000.0f);


                    //lighting
                    effect.DirectionalLight0.DiffuseColor = new Vector3(0.3f, 0.3f, 0.1f);
                    effect.DirectionalLight0.Direction = new Vector3(5, -10, 4);
                    effect.DirectionalLight0.Enabled = true;
                    effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                    effect.DirectionalLight1.DiffuseColor = new Vector3(0, 0, 1);
                    effect.DirectionalLight1.Direction = new Vector3(0, 0, -5);
                    effect.DirectionalLight1.Enabled = true;

                    effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.LightingEnabled = true;
                    //lighting doesn't seem to have an effect on textures - might need a shader program for that
                }
                mesh.Draw();
            }
        }



    }
}
