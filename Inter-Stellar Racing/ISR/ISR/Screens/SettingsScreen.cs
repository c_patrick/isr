﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class SettingsScreen:GameScreen
    {
        int aiPlayers = 1;
        int aiDiff = 0;
        int laps = 1;
        int menuSelect = 0;
        int[] values = {1,1,0};
        String[] aiDifficulty = {"Easy","Medium","Hard"};

        public override void LoadContent(ContentManager content)
        {
            //base.LoadContent();
        }
        public override void Initialize()
        {
            transFade = 0.3f;
            spriteBatch = screenMan.SpriteBatch;
            //throw new NotImplementedException();
        }




        bool pulseUp = true;
        private void updateControls()
        {
            if ((InputState.keyboard.IsKeyDown(Keys.Up) || InputState.gamePads[0].IsButtonDown(Buttons.DPadUp)))
            {
                menuDelay = 0;
                menuSelect--;
                if (menuSelect < 0)
                    menuSelect = 2;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Down) || InputState.gamePads[0].IsButtonDown(Buttons.DPadDown)))
            {
                menuDelay = 0;
                menuSelect++;
                if (menuSelect >= 3)
                    menuSelect = 0;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Right) || InputState.gamePads[0].IsButtonDown(Buttons.DPadRight)))
            {
                menuDelay = 0;
                values[menuSelect]++;
                if (values[menuSelect] >10 && menuSelect == 0)
                    values[menuSelect] = 10;
                else if (values[menuSelect] > 4 && menuSelect == 1)
                    values[menuSelect] = 4;
                else if (values[menuSelect] >= 3 && menuSelect == 2)
                    values[menuSelect] = 2;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Left) || InputState.gamePads[0].IsButtonDown(Buttons.DPadLeft)))
            {
                menuDelay = 0;
                values[menuSelect]--;
                if (values[menuSelect] < 1 && menuSelect != 2)
                    values[menuSelect] = 1;
                if (values[menuSelect] < 0 && menuSelect == 2)
                    values[menuSelect] = 0;
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Enter) || InputState.gamePads[0].IsButtonDown(Buttons.A)))
            {
                GameManager.Laps = values[0];
                GameManager.aiPlayers = values[1];
                
                menuDelay = 0;

                screenMan.startGame(this);
            }
            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                menuDelay = 0;
                nextScreen = 0;
                exiting = true;

            }
        }
        
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            menuDelay++;

            if (menuDelay > 10)
            {
                updateControls();
            }
            
        }
        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            Color[] selectedColor = {Color.Black,Color.White};
            int[] offset = {50,0};

            int font = 0;
            spriteBatch.Draw(ContentLoader.Menu_Elements[2], new Rectangle(0, 75, screenWidth, ContentLoader.Menu_Elements[3].Height), alpha);

            if (menuSelect == 0)
                font = 1;
            spriteBatch.DrawString(ContentLoader.HUD_Font[font], "Number of Laps: " + values[0], new Vector2(screenWidth / 2 - 175 + offset[font], screenHeight / 2 - 150), selectedColor[font]);
              if (menuSelect == 1)
                  font = 1;
              else
                  font = 0;
              spriteBatch.DrawString(ContentLoader.HUD_Font[font], "Number of Opponents: " + values[1], new Vector2(screenWidth / 2 - 175+offset[font], screenHeight / 2), selectedColor[font]);
              if (menuSelect == 2)
                  font = 1;
              else
                  font = 0;
              spriteBatch.DrawString(ContentLoader.HUD_Font[font], "Opponent Difficulty: " + aiDifficulty[values[2]], new Vector2(screenWidth / 2 - 175 + offset[font], screenHeight / 2 + 150), selectedColor[font]);

            spriteBatch.Draw(ContentLoader.Menu_Elements[19], new Rectangle(100, screenHeight - ContentLoader.Menu_Elements[19].Height - 50, ContentLoader.Menu_Elements[19].Width, ContentLoader.Menu_Elements[19].Height), alpha);
            spriteBatch.Draw(ContentLoader.Menu_Elements[20], new Rectangle(screenWidth - ContentLoader.Menu_Elements[20].Width - 100, screenHeight - ContentLoader.Menu_Elements[20].Height - 50, ContentLoader.Menu_Elements[20].Width, ContentLoader.Menu_Elements[20].Height), alpha);
            //spriteBatch.Draw
            //throw new NotImplementedException();
        }

    }
}
