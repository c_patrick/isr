﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    class ControlScreen : GameScreen
    {
        /*
         Need a method to transition between screens.
         something of the form: void toNextScreen(ScreenType,selectedOption,isVisible);
         selectedOption will determine which screen gets drawn or I could just pass the type of menu its going to
         I'm not actually going to remove screen and place them back in. 
         Instead I'll flip between them, setting the update and draw values to true and false as I go along
         
         * Can launch transitions based on whether the game is still running
         * Need a MenuItem class
         * PauseScreen, ControlScreen
         * 
         */

        Texture2D background,menuTitle;
        Texture2D controls1, controls2, controls3;

        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;

        public ControlScreen()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
            
        }

        private void changeScreens()
        {
            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                // should have transitioning (i.e. fading to black) happening here
                this.nextScreen = 0;
                this.exiting = true;
                
            }
        }

        # region Load and Initialize

        public override void LoadContent(ContentManager content)
        {

            spriteBatch = this.screenMan.SpriteBatch;

            menuTitle = ContentLoader.Menu_Elements[3];//content.Load<Texture2D>("MenuItems/menu_bar - heading");
            controls1 = content.Load<Texture2D>("MenuItems/item_controls1");
            //controls2 = content.Load<Texture2D>("MenuItems/item_controls2");
            controls3 = ContentLoader.Menu_Elements[12];

        }
        public override void Initialize()
        {

        }

        # endregion

        #region Update and Draw

        public override void Update(GameTime gameTime)
        {
            //a delay for the buttons
            if (!exiting && !entering)
                changeScreens();
        }
        public override void Draw(GameTime gameTime)
        {
            //spriteBatch.Begin();
            int centre_x = 1366 / 2;
            int centre_y = 768 / 2;
            Color alpha = new Color(255,255,255,transFade);

            // draw the title
            //spriteBatch.Draw(background, new Vector2(0, 0),
            //   new Rectangle(0, 0, 1280, 720), Color.White);
            spriteBatch.Draw(menuTitle, new Rectangle(0, 75, centre_x * 2, menuTitle.Height), Color.White);

            // draw the menu options
            spriteBatch.Draw(controls3,
                new Vector2(centre_x - controls3.Width / 2, centre_y - controls3.Height / 2 + 50), alpha);

            spriteBatch.Draw(controls1,
                new Vector2(centre_x + controls1.Width, centre_y*2 - controls1.Height - 50), alpha);

            //spriteBatch.End();
        }

        #endregion
    }
}