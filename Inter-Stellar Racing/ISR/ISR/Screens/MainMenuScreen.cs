﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    class MainMenuScreen : GameScreen
    {
        /*
         Need a method to transition between screens.
         something of the form: void toNextScreen(ScreenType,selectedOption,isVisible);
         selectedOption will determine which screen gets drawn or I could just pass the type of menu its going to
         I'm not actually going to remove screen and place them back in. 
         Instead I'll flip between them, setting the update and draw values to true and false as I go along
         
         * Can launch transitions based on whether the game is still running
         * Need a MenuItem class
         * PauseScreen, ControlScreen
         * 
         */



        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;
        int menuTime = 0;
        int MenuState = 0;

       // MenuElement title;

        Texture2D menuTitle;
        Texture2D selectionBar;
        Texture2D[,] mainMenuOptions = new Texture2D[1, 1];
        Texture2D[] MainMenuElements = new Texture2D[4];
        int[] elementState = { 0, 1, 1, 1 };
        float modelTime = 0.0f;
        
        public MainMenuScreen()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();
        }

        public override void LoadContent(ContentManager content)
        {
            String[] hudElements = { "ISR - Lap Counter", "ISR - Place Tracker", "ISR - Speedometer", 
                                     "ISR - Position Tracker", "ISR - Visual Position Tracker" };

            String[] elementOptions = { "item1", "item2", "item3", "item4" };


            spriteBatch = this.screenMan.SpriteBatch;

            mainMenuOptions = new Texture2D[2, elementOptions.Length];
            menuTitle = content.Load<Texture2D>("MenuItems/menu_bar - heading");
            selectionBar = content.Load<Texture2D>("MenuItems/selection_bar");
            //Viewport vp = graphics.GraphicsDevice.Viewport;

            // intialising the Main Menu's UI elements
            for (int x = 0; x < MainMenuElements.Length; x++)
            {
                MainMenuElements[x] = content.Load<Texture2D>("MenuItems/background" + (x + 1));
            }
            for (int x = 0; x < elementOptions.Length; x++)
            {
                mainMenuOptions[0, x] = content.Load<Texture2D>("MenuItems/" + elementOptions[x]);
                mainMenuOptions[1, x] = content.Load<Texture2D>("MenuItems/" + elementOptions[x] + "_small");
            }
           // MediaPlayer.Play(content.Load<Song>("isr - no.15"));
        }
        public override void Initialize()
        {
            
        }
        public override void Update(GameTime gameTime)
        {
            
            
            
            //a delay for the buttons
            menuTime++;
            modelTime = (float)gameTime.TotalGameTime.TotalSeconds/5.0f;
            // handle input
            if ((InputState.keyboard.IsKeyDown(Keys.Down) || InputState.gamePads[0].IsButtonDown(Buttons.DPadDown)) && menuTime >= 15)
            {
                menuTime = 0;
                MenuState++;
                if (MenuState == 4)
                    MenuState = 0;
            }

            if ((InputState.keyboard.IsKeyDown(Keys.Up) || InputState.gamePads[0].IsButtonDown(Buttons.DPadUp)) && menuTime >= 15)
            {
                menuTime = 0;
                MenuState--;
                if (MenuState == -1)
                    MenuState = 3;


            }
            // handle a change in the menu state
            switch (MenuState)
            {
                case (0): elementState[0] = 0;
                    elementState[1] = 1;
                    elementState[2] = 1;
                    elementState[3] = 1; break;
                case (1): elementState[0] = 1;
                    elementState[1] = 0;
                    elementState[2] = 1;
                    elementState[3] = 1; break;
                case (2): elementState[0] = 1;
                    elementState[1] = 1;
                    elementState[2] = 0;
                    elementState[3] = 1; break;
                case (3): elementState[0] = 1;
                    elementState[1] = 1;
                    elementState[2] = 1;
                    elementState[3] = 0; break;

            }
            if (!exiting && ! entering)
                changeScreens();

        }
     

        private void changeScreens()
        {

            if ((InputState.keyboard.IsKeyDown(Keys.Back) || InputState.gamePads[0].IsButtonDown(Buttons.B)))
            {
                screenMan.Game.Exit();
            }

            if ((InputState.keyboard.IsKeyDown(Keys.Space) || InputState.gamePads[0].IsButtonDown(Buttons.A)))
            {
                menuTime = 0;
                // should have transitioning (i.e. fading to black) happening here
                // - transitioning happens in game screen
                if (MenuState == 0)
                {
                    GameManager.hPlayers = 1;
                    this.screenMan.startGame(this);
                }
                if (MenuState == 1)
                {
                    GameManager.hPlayers = 1;
                    this.screenMan.changeScreen(this, 4, false);
                }
                    
                else if (MenuState == 2)
                {
                    //GameManager.hPlayers = 2;
                    this.screenMan.changeScreen(this,5,false);
                }
                    
                else if (MenuState == 3)
                    this.screenMan.changeScreen(this, 3, false);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            int[] selection_barPos = { 125, 50, -25, -100 };
            int[] selection_element = { 100, 25, -50, -125 };

            //spriteBatch.Begin();

            int centre_x = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width/2;
            int centre_y = graphics.PreferredBackBufferHeight;

            Color alpha = new Color(255,255,255,transFade);

            // draw background - soon to be obselete
            //   spriteBatch.Draw(MainMenuElements[0], new Vector2(0, 0), Color.White);
            // draw the menu title
            

            spriteBatch.Draw(menuTitle, new Rectangle(0, 75,screenWidth,menuTitle.Height), Color.White);

            // draw the menu options
            spriteBatch.Draw(selectionBar, new Vector2(0, centre_y - (selection_barPos[MenuState])-75), alpha);

            spriteBatch.Draw(mainMenuOptions[elementState[0], 0],
                new Vector2(centre_x / 2 - (centre_x / 4), centre_y - selection_element[0] - 75), alpha);
            spriteBatch.Draw(mainMenuOptions[elementState[1], 1],
                new Vector2(centre_x / 2 - (centre_x / 4), centre_y - selection_element[1] - 75), alpha);
            spriteBatch.Draw(mainMenuOptions[elementState[2], 2],
                new Vector2(centre_x / 2 - (centre_x / 4), centre_y - selection_element[2] - 75), alpha);
            spriteBatch.Draw(mainMenuOptions[elementState[3], 3],
                new Vector2(centre_x / 2 - (centre_x / 4), centre_y - selection_element[3] - 75), alpha);

            spriteBatch.Draw(ContentLoader.Menu_Elements[7],
                new Vector2(screenWidth / 2 + ContentLoader.Menu_Elements[7].Width + 50, screenHeight - 
                    ContentLoader.Menu_Elements[7].Height - 75), alpha);
            
            // spriteBatch.End();
        }

    }
}