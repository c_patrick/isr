﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class ScreenManager : DrawableGameComponent
    {
        Texture2D textureBackground;
        Texture2D MainMenuElement;
        
        SpriteBatch spriteBatch;
        ContentManager content;

        SpriteFont font;
        BlurManager blurManager;
        GraphicsDeviceManager graphics;
        
        List<GameScreen> gameScreens = new List<GameScreen>();
        List<TrackManager> tracks = new List<TrackManager>();

        SoundEffect back;
        float backgroundTime = 0.0f;

        public ScreenManager(Game game, GraphicsDeviceManager graphicsManager) : base(game)
        {
            this.graphics = graphicsManager;
        }

        /// <summary>
        /// Method to return a SpriteBatch used by all the screens
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            set { spriteBatch = value; }
            get { return spriteBatch; }
        }

        public ContentManager contentMan
        {
            set { content = value; }
            get { return content; }
        }

        public void addScreen(GameScreen newScreen, bool update, bool draw)
        {
            newScreen.Drawn = draw;
            newScreen.Updated = update;

            gameScreens.Add(newScreen);
            newScreen.screenManager = this;
        }
        public void changeScreen(GameScreen currentScreen, int nextScreenIndex, bool stillVisible)
        {
            if (nextScreenIndex == 0 || nextScreenIndex == 2)
            back.Play();

            // change the update and draw values
            currentScreen.Drawn = stillVisible;
            currentScreen.Updated = false;// should probably always be false

            // allows the update and draw steps of the next screen to run 
            gameScreens[nextScreenIndex].Drawn = true;
            gameScreens[nextScreenIndex].Updated = true;

            // allows for the next screen to fade in
            gameScreens[nextScreenIndex].EnterScreen = true;

        }

        public void addTrack(TrackManager track)
        {
            tracks.Add(track);
        }
        public void RestartRace()
        {
            gameScreens[gameScreens.ToArray().Length - 1].RestartRace();
        }

        public void startGame(GameScreen currentScreen)
        {
            // creates a new gameplayer screen and switches to it
            Gameplay gameplayScreen = new Gameplay(graphics, tracks[GameManager.TrackSelect]);
            MediaPlayer.Stop();
            // set the track manager for the gameplay screen
            //gameplayScreen.trackManager = ;

            gameplayScreen.LoadContent(content);

            gameplayScreen.trackManager = tracks[GameManager.TrackSelect];
            gameplayScreen.Initialize();
            // adds the gameplay screen to the list of screens 
            addScreen(gameplayScreen, false, false);

            // change screen to the new gamplay screen
            int select = gameScreens.ToArray().Length-1;
            changeScreen(currentScreen,select,false);
        }
        public void endGame(GameScreen currentScreen, int nextScreen)
        {
            // changes the screen and deletes the instance of the current screen
            changeScreen(currentScreen, nextScreen, false);
            // removes the gameplay class from screens - gameplay is the last screen added
            gameScreens.Remove(gameScreens[gameScreens.ToArray().Length - 1]);
            MediaPlayer.Stop();
            MediaPlayer.Play(ContentLoader.MusicElements[0]);
            GameManager.Initialise();// randomises the race ettings for any new quick race
        }
        // draw the background animated image
        public void DrawBackground(GraphicsDevice gd)
        {
            if (gd == null)
            {
                throw new ArgumentNullException("gd");
            }

            const float animationTime = 3.0f;
            const float animationLength = 0.4f;
            const int numberLayers = 3;
            const float layerDistance = 1.0f / numberLayers;

            // normalized time
            float normalizedTime = ((backgroundTime / animationTime) % 1.0f);

            // set render states
            DepthStencilState ds = gd.DepthStencilState;
            BlendState bs = gd.BlendState;
            gd.DepthStencilState = DepthStencilState.DepthRead;
            gd.BlendState = BlendState.AlphaBlend;

            float scale;
            Vector4 color;

            // render all background layers
            for (int i = 0; i < numberLayers; i++)
            {
                if (normalizedTime > 0.5f)
                    scale = 2 - normalizedTime * 2;
                else
                    scale = normalizedTime * 2;
                scale /= 2;
                color = new Vector4(scale, scale, scale, 0);

                scale = 1 + normalizedTime * animationLength;

                blurManager.RenderScreenQuad(gd,
                    BlurTechnique.ColorTexture, textureBackground, color, scale);

                normalizedTime = (normalizedTime + layerDistance) % 1.0f;
            }

            // restore render states
            gd.DepthStencilState = ds;
            gd.BlendState = bs;

        }


        # region Load and Initialize

        public override void Initialize()
        {
            base.Initialize();
            content = Game.Content;
            SoundEffect.MasterVolume = 0.25f;
            GameScreen tempScreen;

            for (int i = 0; i < tracks.ToArray().Length; i++)
            {
                tracks[i].Initialize();
            }

            for (int x = 0; x < gameScreens.ToArray().Length; x++)
            {
                tempScreen = gameScreens.ToArray()[x];
                tempScreen.Initialize();
            }

            back = ContentLoader.SoundElements[0];
            

            //   isInitialized = true;
        }
        public void LoadContent(GraphicsDevice gd)
        {
            content = Game.Content;
            ContentLoader.LoadContent(content);
            textureBackground = content.Load<Texture2D>("background2");
            // create blur manager
            blurManager = new BlurManager(gd,
                content.Load<Effect>("shaders/Blur"),
                512, 512);


          //  mainTheme = content.Load<Song>("ISR - No.15");
            spriteBatch = new SpriteBatch(gd);
            font = content.Load<SpriteFont>("Fonts/TestFont");
         //   MainMenuElement = content.Load<Texture2D>("UI_Elements/Main Menu (1)");

            GameScreen tempScreen;
            for (int x = 0; x < gameScreens.ToArray().Length; x++)
            {
                tempScreen = gameScreens.ToArray()[x];
                tempScreen.LoadContent(content);
            }
           // MediaPlayer.Play(mainTheme);
        }

        # endregion

        # region Update and Draw

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);

            // Updates the state of the controllers
            InputState.Update();

            // Update the state of the screens
            GameScreen tempScreen;
            for (int x = 0; x < gameScreens.ToArray().Length; x++)
            {
                tempScreen = gameScreens.ToArray()[x];
                if (tempScreen.Updated)
                    tempScreen.Logic(gameTime);
            }
            backgroundTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice.Clear(new Color(0.02f, 0.02f, 0.06f));

            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            spriteBatch.Begin();


            DrawBackground(GraphicsDevice);

            GameScreen tempScreen;
            for (int x = 0; x < gameScreens.ToArray().Length; x++)
            {
                tempScreen = gameScreens.ToArray()[x];
                if (tempScreen.Drawn)
                    tempScreen.Draw(gameTime);
            }
            // if (font != null)
            //      spriteBatch.DrawString(font, "Hello", new Vector2(25, 25), Color.White);
            //(MainMenuElement)
            spriteBatch.End();

        }

        # endregion

    }
}
