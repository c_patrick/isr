﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    /// <summary>
    /// Class to load the assets needed
    /// </summary>
    public class ContentLoader
    {
        // store content variables here
        // store 2 track managers for both tracks and pass them to the gameplay class when needed
        static TrackManager[] tracks = new TrackManager[2];

        #region 2D Texture Elements
        static Texture2D[] HUD_Textures = new Texture2D[18];
        static Texture2D[] MENU_Textures = new Texture2D[23];
        #endregion

        #region 3D Model Elements
        static Model[] ShipModels = new Model[9];
        static Model coneModel;
        #endregion


        static Song[] Music_Elements = new Song[4];
        static SoundEffect[] soundEffects = new SoundEffect[4];

        static SpriteFont[] hudFont = new SpriteFont[3];

        #region Audio Elements
        #endregion

        
        #region Accessors_AND_Mutators

        public static SoundEffect[] SoundElements
        {
            get { return soundEffects; }
        }

        public static Song[] MusicElements
        {
            get { return Music_Elements; }
        }

        public static Texture2D[] HUD_Elements
        {
            get { return HUD_Textures; }
        }
        public static Texture2D[] Menu_Elements
        {
            get { return MENU_Textures; }
        }

        public static SpriteFont[] HUD_Font
        {
            get { return hudFont; }
        }

        public static Model Cone_Model
        {
            get { return coneModel; }
        }
        public static Model[] Ship_Models
        {
            get { return ShipModels; }
        }
        public TrackManager CurrentTrack
        {
            set { tracks[GameManager.TrackSelect] = value; }
            get { return tracks[GameManager.TrackSelect]; }

        }

        #endregion
     
        public static void Initialise(Game1 game)
        {
            // initialise the track managers
            tracks[0] = new TrackManager(game, 0);
            tracks[1] = new TrackManager(game, 1);
        }

        public static void LoadContent(ContentManager content)
        {
            // load the content
            String[] musicElements = { "Music/ISR", "Music/on-the-run-01", "Music/power-juice" , "Music/techno-dog"};
            String[] soundElements = { "menu_back", "menu_forwards", "PowerUp_collection","boost"};

            String[] hudElements = { "Pause_MenuBar", "Boost_Button", "Power-Up Menu", "PowerUpMenu_Boost",
                                   "PowerUpMenu_full","PowerUpMenu_full2","PowerUpMenu_mine","PowerUpMenu_mine2",
                                   "PowerUpMenu_rocket","PowerUpMenu_rocket2","PowerUpMenu_doublemine","powerupmenu_doublerocket","GO",
                                   "Start_CountDown (1)","Start_CountDown (2)","Start_CountDown (3)","Start_CountDown (4)","Start_CountDown (5)"};

            String[] menuElements = { "Start Screen","Start_Button",
                                        "Title_Bar","TitleBar_Controls","TitleBar_RaceOver",
                                        "TitleBar_ShipSelect","TitleBar_Track","MainMenu_Controls",
                                        "PauseMenu_Controls","PauseMenu_Option1","PauseMenu_Option2",
                                        "PauseMenu_Option3","Controls_View","Track1","track2","backgroundtransparent",
                                        "Multi_Start","Multi_Controls1","Multi_Controls2","Settings_Controls1","Settings_Controls2",
                                        "RaceEvent_place1","RaceEvent_place2"
                                    };

            String[] ship_names = { "Ship", "Buggie", "Jet" };



            soundEffects[0] = content.Load<SoundEffect>("Sounds/" + soundElements[0]);
            soundEffects[1] = content.Load<SoundEffect>("Sounds/" + soundElements[1]);
            soundEffects[2] = content.Load<SoundEffect>("Sounds/" + soundElements[2]);
            soundEffects[3] = content.Load<SoundEffect>("Sounds/" + soundElements[3]);

            Music_Elements[0] = content.Load<Song>(musicElements[0]);
            Music_Elements[1] = content.Load<Song>(musicElements[1]);
            Music_Elements[2] = content.Load<Song>(musicElements[2]);
            Music_Elements[3] = content.Load<Song>(musicElements[3]);

            hudFont[0] = content.Load<SpriteFont>("Fonts/TestFont");
            hudFont[1] = content.Load<SpriteFont>("Fonts/RacingFont");
            hudFont[2] = content.Load<SpriteFont>("Fonts/OptionsFont");

            // load 3D models
            int count = 0;
            for (int x = 0; x < 3; x++)
                for (int y = 0; y < 3; y++)
                {
                    ShipModels[count] = content.Load<Model>("Models/Ships/" + ship_names[x] + " " + (y+1));
                    count++;
                }
            coneModel = content.Load<Model>("Models/Ships/Cone");

            // load the 2d elements
            for (int x = 0; x < HUD_Textures.Length; x++)
            {
                HUD_Textures[x] = content.Load<Texture2D>("HUD_Elements/" + hudElements[x]);
            }
            for (int x = 0; x < MENU_Textures.Length; x++)
            {
                MENU_Textures[x] = content.Load<Texture2D>("Screen_Elements/"+menuElements[x]);
            }

            //load the 3d elements
        }
    }
}
