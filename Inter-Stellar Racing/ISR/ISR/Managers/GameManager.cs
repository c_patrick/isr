﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISR
{
    class GameManager
    {
        private static int numberOfHPlayers = 1;
        private static int numberOfAIPlayers = 1;
        private static int numberOfLaps = 3;
        private static int trackSelect = 1;
        private static int aiDifficulty = 1;

        private static int[] hModels = new int[4];
        private static List<Ship> positioning = new List<Ship>();
        static TrackManager trackMan;
        static Random randomGen = new Random();

        public static void Initialise()
        {
            trackSelect = randomGen.Next(2);
            numberOfLaps = randomGen.Next(10) + 1;
            numberOfHPlayers = randomGen.Next(4)+1;
            numberOfAIPlayers = randomGen.Next(4);
            aiDifficulty = randomGen.Next(3);

            for (int i = 0; i < 4; i++)
                hModels[i] = randomGen.Next(9);
        }

        public static List<Ship> Positioning
        {
            get { return positioning; }
        }

        public static int Random4
        {
            get { return randomGen.Next(4); }
        }

        public static int Random100
        {
            get { return randomGen.Next(100); }
        }

        public static int AIdifficulty
        {
            set { aiDifficulty = value; }
            get { return aiDifficulty; }
        }

        public static void InitaliseTrackMan(Game1 game)
        {
            trackMan = new TrackManager(game,trackSelect);
        }

        public static int TrackSelect
        {
            set { trackSelect = value; }
            get { return trackSelect; }
        }
        
        public static int Laps
        {
            set { numberOfLaps = value; }
            get { return numberOfLaps; }
        }
        public static int[] humanModels
        {
            set { hModels = value; }
            get { return hModels; }
        }
        public static int hPlayers
        {
            set { numberOfHPlayers = value; }
            get { return numberOfHPlayers; }
        }
        public static int aiPlayers
        {
            set { numberOfAIPlayers = value;  }
            get { return numberOfAIPlayers; }
        }
    }
}
