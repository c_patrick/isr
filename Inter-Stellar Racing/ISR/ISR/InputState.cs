﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ISR
{
    public class InputState
    {
        public static KeyboardState keyboard;
        public static readonly GamePadState[] gamePads = new GamePadState[4];


        public static void Update()
        {

            //Updating the input states
            keyboard = Keyboard.GetState();
            gamePads[0] = GamePad.GetState(PlayerIndex.One);
            gamePads[1] = GamePad.GetState(PlayerIndex.Two);
            gamePads[2] = GamePad.GetState(PlayerIndex.Three);
            gamePads[3] = GamePad.GetState(PlayerIndex.Four);

        }
        // should have a methiod to checkwhther a specific state has been reached across all gamepads 
    }
}
