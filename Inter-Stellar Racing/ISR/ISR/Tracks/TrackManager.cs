﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Globalization;

namespace ISR
{
    // problem in that track mananger is created with a specific track in mind and this must be passed through the constructor

    public class TrackManager : DrawableGameComponent
    {
        #region Constants
        Vector3 straightDim = new Vector3(40.0f, 0.0f, 90.0f);
        Vector3 shortStraightDim = new Vector3(40.0f, 0.0f, 55.0f);
        Vector3 cornerDim = new Vector3(61.0f, 0.0f, 63.0f);
        Vector3 halfCornerDim = new Vector3(62.0f, 0.0f, 71.0f);
        #endregion

        #region Global Variables
        // track number
        int trackNumber;
        public int trackLength;
        #endregion

        #region Objects
        // objects
        Model straightModel;
        Model shortStraightModel;
        Model cornerModel;
        Model halfCornerModel;
        Model pillarModel;
        Model minePowerModel;
        Model mineModel;
        Model rocketPowerModel;
        Model rocketModel;

        List<TrackPiece> track; // fill with track pieces
        List<TrackPiece> pillars; //fill with pillars - must be separate as track list length is important
        List<Powerup> powerUps; //fill with powerups - assuming we load them in through the track
        public List<float> splittimes;
        public List<Vector3> checkpoints;
        #endregion

        #region Constructors
        // constructor
        public TrackManager(Game game, int trackChoice)
            : base(game)
        {
            trackNumber = trackChoice;
        }
        #endregion

        #region Overridden Methods
        // initialising
        public override void Initialize()
        {

            base.Initialize();

            track = new List<TrackPiece>();
            pillars = new List<TrackPiece>();
            powerUps = new List<Powerup>();
            checkpoints = new List<Vector3>();
            if (trackNumber == 1)
            {
                //Load("Content\\Tracks\\1.scn");
                Load("Content/Tracks/track1.txt");

                //loading power-ups
                Powerup minePickUp = new Powerup(new Vector3(12.5f, 5, -400), minePowerModel, mineModel, 6);
                Powerup minePickUp1 = new Powerup(new Vector3(1280.5f, 142.5f, -1706.2f), minePowerModel, mineModel, 6);
                Powerup minePickUp2 = new Powerup(new Vector3(1546, 142.5f, -2141.6f), minePowerModel, mineModel, 6);
                Powerup minePickUp3 = new Powerup(new Vector3(1890.5f, 142.5f, 1086), minePowerModel, mineModel, 6);
                Powerup minePickUp4 = new Powerup(new Vector3(390.4f, 142.5f, 2004.3f), minePowerModel, mineModel, 6);
                Powerup minePickUp5 = new Powerup(new Vector3(-244.9f, 142.5f, 1084.2f), minePowerModel, mineModel, 6);
                Powerup minePickUp6 = new Powerup(new Vector3(-1042, 142.5f, 1502.3f), minePowerModel, mineModel, 6);
                Powerup minePickUp7 = new Powerup(new Vector3(-2152, 71, 1023.4f), minePowerModel, mineModel, 6);
                Powerup minePickUp8 = new Powerup(new Vector3(-698, 71, 528.9f), minePowerModel, mineModel, 6);
                Powerup minePickUp9 = new Powerup(new Vector3(-693.7f, 71, 137.8f), minePowerModel, mineModel, 6);

                Powerup rocketPickUp = new Powerup(new Vector3(-14, 94.6f, -1065), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp1 = new Powerup(new Vector3(353, 94.6f, -1338), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp2 = new Powerup(new Vector3(1546, 142.5f, -2113.5f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp3 = new Powerup(new Vector3(1863.2f, 142.5f, -1639.5f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp4 = new Powerup(new Vector3(1867.4f, 142.5f, 2015.4f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp5 = new Powerup(new Vector3(-642.8f, 142.5f, 1479.8f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp6 = new Powerup(new Vector3(-1502.7f, 130, 1638.5f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp7 = new Powerup(new Vector3(-1453, 71, 813), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp8 = new Powerup(new Vector3(-644, 71, 134.76f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp9 = new Powerup(new Vector3(-270.3f, 70.9f, 688.7f), rocketPowerModel, rocketModel, 7);
                Powerup rocketPickUp10 = new Powerup(new Vector3(-13, 5, 327.36f), rocketPowerModel, rocketModel, 7);

                powerUps.Add(minePickUp);
                powerUps.Add(minePickUp1);
                powerUps.Add(minePickUp2);
                powerUps.Add(minePickUp3);
                powerUps.Add(minePickUp4);
                powerUps.Add(minePickUp5);
                powerUps.Add(minePickUp6);
                powerUps.Add(minePickUp7);
                powerUps.Add(minePickUp8);
                powerUps.Add(minePickUp9);

                powerUps.Add(rocketPickUp);
                powerUps.Add(rocketPickUp1);
                powerUps.Add(rocketPickUp2);
                powerUps.Add(rocketPickUp3);
                powerUps.Add(rocketPickUp4);
                powerUps.Add(rocketPickUp5);
                powerUps.Add(rocketPickUp6);
                powerUps.Add(rocketPickUp7);
                powerUps.Add(rocketPickUp8);
                powerUps.Add(rocketPickUp9);
                powerUps.Add(rocketPickUp10);

                checkpoints.Add(new Vector3(1711, 142, -2128));
                checkpoints.Add(new Vector3(1877, 142, 1245));
                checkpoints.Add(new Vector3(-238, 142, 1067));
                checkpoints.Add(new Vector3(-2047, 71, 811));
                checkpoints.Add(new Vector3(0, 5, -10));
                splittimes = new List<float>() { 0, 0, 0, 0, 0 };
            }
            else
            {
                //test track
                //types: 1 = straight, 2 = short straight, 3 = 90 degree corner, 4 = 70 degree corner
                //type 5 = pillar for now
                //dimensions: 1 = (38, 87); 2 = (38, 50); 3 = (61, 63); 4 = (62, 71)
                TrackPiece floor1 = new TrackPiece(new Vector3(0, 0, 0), straightModel, 1, 40, 87, 0, MathHelper.Pi, 1);
                track.Add(floor1);
                TrackPiece corner1 = new TrackPiece(new Vector3(-11.7f, 0, -78f), halfCornerModel, 4, 62, 71, 0, 0, 2);
                track.Add(corner1);
                TrackPiece corner2 = new TrackPiece(new Vector3(-59.3f, 0, -112.4f), halfCornerModel, 4, 62, 71, 0, MathHelper.Pi, 3);
                track.Add(corner2);
                TrackPiece floor2 = new TrackPiece(new Vector3(-71, 0, -171.9f), shortStraightModel, 2, 40, 50, 0, MathHelper.Pi, 4);
                track.Add(floor2);
                TrackPiece corner3 = new TrackPiece(new Vector3(-82.8f, 0, -226.9f), cornerModel, 3, 61, 63, 0, 0, 5);
                track.Add(corner3);
                TrackPiece floor3 = new TrackPiece(new Vector3(-156.5f, 0.01f, -238.3f), straightModel, 1, 40, 87, 0, -MathHelper.PiOver2, 6);
                track.Add(floor3);
                TrackPiece corner4 = new TrackPiece(new Vector3(-230, 0, -226.5f), cornerModel, 3, 61, 63, 0, MathHelper.PiOver2, 7);
                track.Add(corner4);
                TrackPiece floor4 = new TrackPiece(new Vector3(-241.4f, 0, -152.5f), straightModel, 1, 40, 87, 0, 0, 8);
                track.Add(floor4);
                TrackPiece floor5 = new TrackPiece(new Vector3(-241.4f, 0, -66), straightModel, 1, 40, 87, 0, 0, 9);
                track.Add(floor5);
                TrackPiece floor6 = new TrackPiece(new Vector3(-241.4f, 0, 2f), shortStraightModel, 2, 40, 50, 0, 0, 10);
                track.Add(floor6);
                TrackPiece corner5 = new TrackPiece(new Vector3(-229.6f, 0, 57.1f), cornerModel, 3, 61, 63, 0, MathHelper.Pi, 11);
                track.Add(corner5);
                TrackPiece floor7 = new TrackPiece(new Vector3(-155.8f, 0, 68.4f), straightModel, 1, 40, 87, 0, MathHelper.PiOver2, 12);
                track.Add(floor7);
                TrackPiece floor8 = new TrackPiece(new Vector3(-69.25f, 0, 68.4f), straightModel, 1, 40, 87, 0, MathHelper.PiOver2, 13);
                track.Add(floor8);
                TrackPiece corner6 = new TrackPiece(new Vector3(-11.5f, 0.001f, 56.5f), cornerModel, 3, 61, 63, 0, -MathHelper.PiOver2, 14);
                track.Add(corner6);

                //length of track
                trackLength = 14;

                //temporary measure - will have a pillar object (maybe; this does actually work quite well)
                TrackPiece pillar1 = new TrackPiece(new Vector3(-22, -5, 0), pillarModel, 5, 10, 10, 0, 0, -1);
                TrackPiece pillar2 = new TrackPiece(new Vector3(22, -5, 0), pillarModel, 5, 10, 10, 0, 0, -1);
                pillars.Add(pillar1);
                pillars.Add(pillar2);

                Powerup minePickUp = new Powerup(new Vector3(0, 5, -30), minePowerModel, mineModel, 6);
                Powerup rocketPickUp = new Powerup(new Vector3(-190, 5, -240), rocketPowerModel, rocketModel, 7);
                powerUps.Add(minePickUp);
                powerUps.Add(rocketPickUp);

                checkpoints.Add(new Vector3(0, 5, -10));
                checkpoints.Add(new Vector3(-136, 5, -234));
                splittimes = new List<float>() { 0, 0 };
            }
        }

        // loading content
        protected override void LoadContent()
        {
            straightModel = Game.Content.Load<Model>(@"Models/Tracks/Straight");
            shortStraightModel = Game.Content.Load<Model>(@"Models/Tracks/ShortStraight");
            cornerModel = Game.Content.Load<Model>(@"Models/Tracks/90turn");
            halfCornerModel = Game.Content.Load<Model>(@"Models/Tracks/45turn");
            pillarModel = Game.Content.Load<Model>(@"Models/Tracks/Pillar");
            minePowerModel = Game.Content.Load<Model>(@"Models/PowerUps/Mine power up");
            mineModel = Game.Content.Load<Model>(@"Models/PowerUps/Mine object");
            rocketPowerModel = Game.Content.Load<Model>(@"Models/PowerUps/Rocket power up");
            rocketModel = Game.Content.Load<Model>(@"Models/PowerUps/Rocket object");

            base.LoadContent();
        }

        // unloading content
        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        //returning the powerups list
        public List<Powerup> getPowerups()
        {
            return powerUps;
        }

        // the update method
        public override void Update(GameTime gameTime)
        {
            double elapsed = gameTime.ElapsedGameTime.TotalMilliseconds;
            foreach (Powerup p in powerUps)
                p.Update(elapsed);
            base.Update(gameTime);
        }

        #endregion

        #region Other Methods
        // draw the track (only draw what is visible in that specific viewport)
        public void DrawTrack(Camera camera, int trackNumber)
        {

            if ((Gameplay.gameState == 1 || Gameplay.gameState == 2))
            {
                //could also try drawing everything within a certain distance of the ship (easy but might be more costly)

                List<Vector3> positions = new List<Vector3>();
                //drawing the track
                for (int i = trackNumber - 2; i < trackNumber + 7; i++)
                {
                    int num = i;
                    if (i == -1)
                        num = trackLength - 1;
                    else if (i == 0)
                        num = trackLength;
                    else if (i > trackLength)
                        num = i - trackLength;
                    DrawModel(findPiece(num).model, findPiece(num).world, camera);
                    positions.Add(findPiece(num).position);
                }

                //drawing the pillars
                foreach (TrackPiece pillar in pillars)
                {
                    for (int i = 0; i < positions.Count(); i++)
                    {
                        if (Vector3.Distance(positions[i], pillar.position) < 180)
                        {
                            DrawModel(pillar.model, pillar.world, camera);
                            break;
                        }
                    }
                }
                //drawing the powerups
                foreach (Powerup pu in powerUps)
                {
                    for (int i = 0; i < positions.Count(); i++)
                    {
                        if (Vector3.Distance(positions[i], pu.position) < 180)
                        {
                            if (pu.acquired == false)
                                DrawModel(pu.pickModel, pu.world, camera);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// get the surrounding track pieces of a ship
        /// </summary>
        public List<TrackPiece> SurroundingTrackPieces(int number)
        {
            List<TrackPiece> pieces = new List<TrackPiece>();
            if (number == 1)
            {
                pieces.Add(findPiece(trackLength));
                pieces.Add(findPiece(1));
                pieces.Add(findPiece(2));
            }
            else if (number == trackLength)
            {
                pieces.Add(findPiece(number - 1));
                pieces.Add(findPiece(number));
                pieces.Add(findPiece(1));
            }
            else
            {
                pieces.Add(findPiece(number - 1));
                pieces.Add(findPiece(number));
                pieces.Add(findPiece(number + 1));
            }
            return pieces;
        }

        //locating the a track piece
        public TrackPiece findPiece(int number)
        {
            TrackPiece piece = new TrackPiece(new Vector3(-50, -50, -50), straightModel, 1, 40, 80, 0, 0, 10);//random
            for (int i = 0; i < track.Count; i++)
            {
                if (track[i].pieceNumber == number)
                    piece = track[i];
            }
            return piece;
        }

        /// <summary>
        /// seperate draw method for drawing models
        /// </summary>
        private void DrawModel(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                    if (model == straightModel || model == shortStraightModel || model == cornerModel || model == halfCornerModel)
                    {
                        //lighting
                        if (trackNumber == 1)
                        {
                            effect.DirectionalLight0.DiffuseColor = new Vector3(0.1f, 0.1f, 0.4f);
                            effect.DirectionalLight0.Direction = new Vector3(0, -10f, 0);
                            effect.DirectionalLight0.Enabled = true;
                            effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.2f);
                        }
                        else
                        {
                            effect.DirectionalLight0.DiffuseColor = new Vector3(0.4f, 0.3f, 0.1f);
                            effect.DirectionalLight0.Direction = new Vector3(0, -10f, 0);
                            effect.DirectionalLight0.Enabled = true;
                            effect.DirectionalLight0.SpecularColor = new Vector3(0.2f, 0.1f, 0.0f);
                        }

                        //effect.DirectionalLight1.DiffuseColor = new Vector3(0.3f, 0.3f, 0.5f);
                        //effect.DirectionalLight1.Direction = new Vector3(-5, -0.5f, 10);
                        //effect.DirectionalLight1.Enabled = true;
                        //effect.DirectionalLight1.SpecularColor = new Vector3(0.1f, 0.1f, 0.2f);

                        //effect.DirectionalLight2.DiffuseColor = new Vector3(0.3f, 0.3f, 0.5f);
                        //effect.DirectionalLight2.Direction = new Vector3(-5, -0.5f, -10);
                        //effect.DirectionalLight2.Enabled = true;
                        //effect.DirectionalLight2.SpecularColor = new Vector3(0.1f, 0.1f, 0.2f);

                        effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                        effect.LightingEnabled = true;
                    }
                    else
                    {
                        //lighting
                        effect.DirectionalLight0.DiffuseColor = new Vector3(0.3f, 0.3f, 0.3f);
                        effect.DirectionalLight0.Direction = new Vector3(5, 10, 4);
                        effect.DirectionalLight0.Enabled = true;
                        effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                        effect.DirectionalLight1.DiffuseColor = new Vector3(0.2f, 0.2f, 0.2f);
                        effect.DirectionalLight1.Direction = new Vector3(-5, -10, -4);
                        effect.DirectionalLight1.Enabled = true;
                        effect.DirectionalLight0.SpecularColor = new Vector3(0.1f, 0.1f, 0.1f);

                        effect.DirectionalLight2.DiffuseColor = new Vector3(0, 0.2f, 0.7f);
                        effect.DirectionalLight2.Direction = new Vector3(-2, -10, -2);
                        effect.DirectionalLight2.Enabled = true;
                        effect.DirectionalLight2.SpecularColor = new Vector3(0.3f, 0.1f, 0.1f);

                        effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                        effect.LightingEnabled = true;
                    }
                }
                mesh.Draw();
            }
        }




        /// <summary>
        /// parse string to vector3
        /// </summary>
        private Vector3 ParseVector3(string value)
        {
            string[] attributeSplit = value.Split();

            Vector3 parsedVector = new Vector3(float.Parse(attributeSplit[0], CultureInfo.InvariantCulture),
                                               float.Parse(attributeSplit[1], CultureInfo.InvariantCulture),
                                               float.Parse(attributeSplit[2], CultureInfo.InvariantCulture));

            return parsedVector;
        }

        /// <summary>
        /// parse string to quaternion
        /// </summary>
        private Quaternion ParseQuaternion(string value)
        {
            string[] attributeSplit = value.Split();

            Quaternion parsedVector = new Quaternion(float.Parse(attributeSplit[0], CultureInfo.InvariantCulture),
                                                     float.Parse(attributeSplit[1], CultureInfo.InvariantCulture),
                                                     float.Parse(attributeSplit[2], CultureInfo.InvariantCulture),
                                                     float.Parse(attributeSplit[3], CultureInfo.InvariantCulture));

            return parsedVector;
        }

        /// <summary>
        /// Parse string to model object 
        /// </summary>
        /// <param name="value">The name of the model</param>
        /// <returns>The model object corresponding to the string</returns>
        private Model ParseModel(string value)
        {
            string name = value.Substring(value.LastIndexOf('\\') + 1);

            // note: add more cases later
            switch (name)
            {
                case "45turn":
                    return halfCornerModel;
                case "90turn":
                    return cornerModel;
                case "ShortStraight":
                    return shortStraightModel;
                case "Straight":
                    return straightModel;
                case "Pillar":
                    return pillarModel;
            }
            return null;
        }

        /// <summary>
        /// Parse string to model type 
        /// </summary>
        /// <param name="value">The type of the model</param>
        /// <returns>The model type corresponding to the string</returns>
        private int ParseType(string value)
        {
            string name = value.Substring(value.LastIndexOf('\\') + 1);

            // note: add more cases later
            switch (name)
            {
                case "45turn":
                    return 4;
                case "90turn":
                    return 3;
                case "ShortStraight":
                    return 2;
                case "Straight":
                    return 1;
                case "Pillar":
                    return 5;
            }

            return 6; //will remove later
        }

        /// <summary>
        /// Parse string to model dimensions 
        /// </summary>
        /// <param name="value">The dimension of the model</param>
        /// <returns>The model dimension corresponding to the string</returns>
        private Vector3 ParseDimension(string value)
        {
            string name = value.Substring(value.LastIndexOf('\\') + 1);

            // note: add more cases later
            switch (name)
            {
                case "45turn":
                    return halfCornerDim;
                case "90turn":
                    return cornerDim;
                case "ShortStraight":
                    return shortStraightDim;
                case "Straight":
                    return straightDim;
                case "Pillar":
                    return straightDim;
            }

            return straightDim;
        }

        /// <summary>
        /// Clear all scene objects
        /// </summary>
        private void Clear()
        {
            track.Clear();
        }

        /// <summary>
        /// Attempts to parse a *.scn file
        /// </summary>
        private void Load(string path)
        {
            Clear();
            //Note: when converting from scn to txt file - remove first 2 and last 2 lines
            string line;
            StreamReader sr = new StreamReader(path);
            while ((line = sr.ReadLine()) != null)
            {
                string name = line.Substring(line.IndexOf("Name=") + 6);
                name = name.Substring(0, name.IndexOf("\""));
                string assetPath = line.Substring(line.IndexOf("AssetPath=") + 11);
                assetPath = assetPath.Substring(0, assetPath.IndexOf("\""));
                string positionString = line.Substring(line.IndexOf("Position=") + 10);
                positionString = positionString.Substring(0, positionString.IndexOf("\""));
                Vector3 position = ParseVector3(positionString);
                string rotationString = line.Substring(line.IndexOf("Rotation=") + 10);
                rotationString = rotationString.Substring(0, rotationString.IndexOf("\""));
                Quaternion rotation = ParseQuaternion(rotationString);

                TrackPiece tp = new TrackPiece(position, ParseModel(assetPath), ParseType(assetPath), int.Parse(name), ParseDimension(assetPath), rotation);
                if (ParseType(assetPath) == 5)
                    pillars.Add(tp);
                else
                    track.Add(tp);
                //don't bother having powerups from file, load manually

            }
            sr.Close();

            trackLength = 207; //for some reason count doesn't work here :S even count() + 1
        }

        #endregion

    }
}
