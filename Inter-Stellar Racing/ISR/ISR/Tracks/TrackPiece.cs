﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ISR
{
    //defines the properties of an individual track piece
    public class TrackPiece
    {
        #region Properties
        //properties
        public Vector3 position;
        public Model model;
        public float pitch;
        public float yaw; //do not add roll - gimbal lock
        public int pieceNumber;
        public int trackType;

        //matrices
        public Matrix world;
        public Matrix rotationMatrix;

        //points for boundary calculations
        Vector3 point0;
        public Vector3 topLeft;
        public Vector3 bottomRight;
        #endregion

        #region Constructors
        //constructor
        public TrackPiece(Vector3 location, Model trackModel, int type, float width, float length, float pitchValue, float yawValue, int trackNumber)
        {
            //assigning properties
            position = location;
            model = trackModel;
            pitch = pitchValue;
            yaw = yawValue;
            trackType = type;
            pieceNumber = trackNumber;

            //initialising the matrix
            world = Matrix.Identity;
            world.Up = Vector3.Up;
            world.Forward = Vector3.Forward;
            world.Right = Vector3.Right;
            rotationMatrix = Matrix.CreateRotationX(pitch) * Matrix.CreateRotationY(yaw);
            world *= rotationMatrix;
            world.Translation = position;

            //bounding points
            topLeft = new Vector3(-(width / 2), 0, -(length / 2));
            bottomRight = new Vector3(width / 2, 0, length / 2);

            //getting point 0 for the height mapping equation
            point0 = Vector3.Transform(topLeft, rotationMatrix) + position;
        }
        //constructor 2
        public TrackPiece(Vector3 location, Model trackModel, int type, int pnumber, Vector3 dimension, Quaternion rotation)
        {
            //assigning properties
            position = location;
            model = trackModel;
            pieceNumber = pnumber;
            toRadians(rotation);
            trackType = type;

            //initialising the matrix
            world = Matrix.Identity;
            world.Up = Vector3.Up;
            world.Forward = Vector3.Forward;
            world.Right = Vector3.Right;
            rotationMatrix = Matrix.CreateRotationX(pitch) * Matrix.CreateRotationY(yaw);
            world *= rotationMatrix;
            world.Translation = position;

            //bounding points
            topLeft = new Vector3(-(dimension.X / 2), 0, -(dimension.Z / 2));
            bottomRight = new Vector3(dimension.X / 2, 0, dimension.Z / 2);

            //getting point 0 for the height mapping equation
            point0 = Vector3.Transform(topLeft, rotationMatrix) + position;
        }
        #endregion

        #region Height mapping
        //height mapping for the track piece
        public float getHeight(Vector3 shipPosition)
        {
            //noraml of the plane
            Vector3 normal = Vector3.TransformNormal(Vector3.Up, rotationMatrix);

            //calculating the y value on the plane at the ship's x and z values
            float y = ((-(normal.Z * (shipPosition.Z - point0.Z)) - (normal.X * (shipPosition.X - point0.X))) / normal.Y) + point0.Y;

            return y;
        }
        #endregion

        #region quaternion to radians
        //converting quaternions to radians
        private void toRadians(Quaternion q)
        {
            const float Epsilon = 0.0009765625f;
            const float Threshold = 0.5f - Epsilon;

            float XY = q.X * q.Y;
            float ZW = q.Z * q.W;

            float TEST = XY + ZW;

            if (TEST < -Threshold || TEST > Threshold)
            {
                int sign = Math.Sign(TEST);

                yaw = sign * 2 * (float)Math.Atan2(q.X, q.W);
                pitch = sign * MathHelper.PiOver2;
            }
            else
            {
                float XX = q.X * q.X;
                float XZ = q.X * q.Z;
                float XW = q.X * q.W;

                float YY = q.Y * q.Y;
                float YW = q.Y * q.W;
                float YZ = q.Y * q.Z;

                float ZZ = q.Z * q.Z;

                yaw = (float)Math.Atan2(2 * YW - 2 * XZ, 1 - 2 * YY - 2 * ZZ);
                pitch = (float)Math.Atan2(2 * XW - 2 * YZ, 1 - 2 * XX - 2 * ZZ);
            }
        }

        #endregion
    }
}
